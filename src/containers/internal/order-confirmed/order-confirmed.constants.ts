export const TEMP_ORDERS: any[] = [
  {
    id: "3064928436327",
    email: "payav60214@izzum.com",
    closed_at: "2021-01-11T21:04:55+01:00",
    created_at: "2021-01-11T21:04:53+01:00",
    updated_at: "2021-01-11T21:04:55+01:00",
    number: 41,
    token: "dbdcee78fa85427b0488b2f8268bfbb1",
    total_price: 260,
    subtotal_price: 260,
    total_weight: 0,
    total_tax: 0,
    currency: "EUR",
    financial_status: "paid",
    confirmed: true,
    total_discounts: 0,
    total_line_items_price: 260,
    cart_token: null,
    buyer_accepts_marketing: false,
    name: "#1041",
    cancelled_at: null,
    cancel_reason: null,
    total_price_usd: 316.83,
    checkout_token: null,
    processed_at: "2021-01-11T21:04:53+01:00",
    app_id: 3905871,
    order_number: 1041,
    fulfillment_status: "fulfilled",
    contact_email: "payav60214@izzum.com",
    line_items: [
      {
        price: "44.00",
        product_id: "4381761044583",
        quantity: 3,
        title: "Long Sleeve Cotton Top",
      },
      {
        price: "68.00",
        product_id: "4381761929319",
        quantity: 1,
        title: "Ocean Blue Shirt",
        __typename: "ShopifyCartItem",
      },
      {
        price: "60.00",
        product_id: "4381760749671",
        quantity: 1,
        title: "Blue Silk Tuxedo",
        __typename: "ShopifyCartItem",
      },
    ],
  },
  {
    id: "3158720807104",
    email: "payav60214@izzum.com",
    closed_at: "2021-01-11T15:04:54-05:00",
    created_at: "2021-01-11T15:04:53-05:00",
    updated_at: "2021-01-11T15:04:54-05:00",
    number: 12,
    token: "43aa1364d11a6a7951dc57755872b813",
    total_price: 118,
    subtotal_price: 118,
    total_weight: 0,
    total_tax: 0,
    currency: "EUR",
    financial_status: "paid",
    confirmed: true,
    total_discounts: 0,
    total_line_items_price: 118,
    cart_token: null,
    buyer_accepts_marketing: false,
    name: "#1012",
    cancelled_at: null,
    cancel_reason: null,
    total_price_usd: 143.79,
    checkout_token: null,
    processed_at: "2021-01-11T15:04:53-05:00",
    app_id: 3905871,
    order_number: 1012,
    fulfillment_status: "fulfilled",
    contact_email: "payav60214@izzum.com",
    line_items: [
      {
        price: "59.00",
        product_id: "6121763340480",
        quantity: 2,
        title: "Floral White Top",
        __typename: "ShopifyCartItem",
      },
    ],
  },
];
