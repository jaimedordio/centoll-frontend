import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";

export const OrderConfirmedStyle = styled.div`
  max-width: 800px;
  margin: 0 auto;

  .order-confirmed-hero {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin: 32px 0 56px;
    text-align: center;

    &__icon {
      padding: 16px;
      background-color: ${colors.lowOpacity.success};
      border-radius: 100%;
      margin-bottom: 20px;
    }
  }

  .order-confirmed-body {
    display: flex;
    align-items: flex-start;

    .order-confirmed-body-sections {
      flex: 1;

      .order-confirmed-body-sections-card {
        background-color: ${colors.bn.white};
        border: 1px solid ${colors.bn.gray_200};
        border-radius: 12px;
        padding: 20px;

        &:not(:last-child) {
          margin-bottom: 16px;
        }

        .order-confirmed-body-sections-card-heading {
          display: flex;
          justify-content: space-between;
          align-items: center;
          margin-bottom: 16px;

          &__text {
            flex: 1;
            margin-top: 0;
          }

          &__action {
            margin-left: 20px;
          }
        }

        .order-confirmed-body-sections-card-content {
        }
      }
    }
  }
`;
