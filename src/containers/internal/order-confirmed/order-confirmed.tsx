import React, { useEffect, useState } from "react";
import { IoIosCheckmark } from "react-icons/io";
import { useHistory, useLocation } from "react-router-dom";
import { colors } from "../../../assets/colors/colors";
import SVGIcon from "../../../components/functional/svg-icon/svg-icon";
import { BodyDefaultText } from "../../../components/styled/text/body-text";
import {
  Heading4Text,
  Heading5Text,
  Heading6Text,
} from "../../../components/styled/text/heading-text";
import { Order } from "../../../types/gql";
import { ILocation } from "../../../types/util_types";
import { OrderConfirmedStyle } from "./order-confirmed.style";

const OrderConfirmed: React.FC = () => {
  const history = useHistory();
  const { state } = useLocation<ILocation>();

  const [placedOrders, setPlacedOrders] = useState<Order[]>([]);

  useEffect(() => {
    setPlacedOrders(state.orders);
    return () => {
      history.replace({ ...history.location, state: {} });
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <OrderConfirmedStyle>
      <div className="order-confirmed-hero">
        <div className="order-confirmed-hero__icon">
          <SVGIcon size="50px" color={colors.states.standard.success}>
            <IoIosCheckmark />
          </SVGIcon>
        </div>
        <div className="order-confirmed-hero__text">
          <Heading6Text className="order-confirmed-hero__text">
            Orders have been created!
          </Heading6Text>
          <BodyDefaultText
            weight={300}
            color={colors.bn.gray_600}
            className="order-confirmed-hero__text"
          >
            You will receive the details on your inbox
          </BodyDefaultText>
        </div>
      </div>
      <div className="order-confirmed-body">
        <div className="order-confirmed-body-sections">
          {placedOrders.length > 0
            ? placedOrders.map((order: Order, orderIndex: number) => (
                <div
                  className="order-confirmed-body-sections-card"
                  key={`order-${orderIndex}`}
                >
                  <div className="order-confirmed-body-sections-card-heading">
                    <Heading4Text className="order-confirmed-body-sections-card-heading__text">
                      Order {order.name}
                    </Heading4Text>
                    <Heading5Text>AAaaAA</Heading5Text>
                  </div>
                  <div className="order-confirmed-body-sections-card-content">
                    <>
                      {console.log(`Order ${orderIndex}: `, order)}
                      {/* {order?.orders?.map((shopifyOrder) =>
                        shopifyOrder?.line_items?.map((item, itemIndex) => (
                          <LineItem
                            key={`item-${itemIndex}`}
                            title={item?.title!}
                            image={item?.image!}
                            price={item?.price!}
                            productId={item?.variant_id!}
                            quantity={item?.quantity!}
                          />
                        ))
                      )} */}
                    </>
                  </div>
                </div>
              ))
            : null}
        </div>
      </div>
    </OrderConfirmedStyle>
  );
};

export default OrderConfirmed;
