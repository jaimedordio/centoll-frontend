import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";

export const OrderHistoryStyle = styled.div`
  .order-history-title {
    margin-bottom: 24px;
  }

  .order-history-filters {
    margin-bottom: 24px;
  }

  .order-history-body {
    .order-history-body-card {
      padding: 24px;
      background-color: ${colors.bn.white};
      border: 1px solid ${colors.bn.gray_200};
      border-radius: 12px;
      overflow: hidden;

      .order-history-body-card-heading {
        margin-bottom: 16px;
      }

      .order-history-body-card-content {
        .order-history-body-card-content-items {
          border-top: 1px solid ${colors.bn.gray_200};
          border-bottom: 1px solid ${colors.bn.gray_200};
          padding: 24px 0;

          &__item {
            &:not(:last-child) {
              margin-bottom: 16px;
            }
          }
        }

        .order-history-body-card-content-actions {
          margin-top: 16px;
        }
      }
    }
  }
`;
