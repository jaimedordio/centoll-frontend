import { useQuery } from "@apollo/react-hooks";
import {
  Heading,
  Spinner,
  Table,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  VStack,
} from "@chakra-ui/react";
import moment from "moment";
import React from "react";
import { ORDERS__QUERY } from "../../../gql/queries";
import { CentollOrder } from "../../../types/gql";

const OrderHistory: React.FC = () => {
  const { loading: ordersLoading, data: ordersData } = useQuery<{
    getOrders: CentollOrder[];
  }>(ORDERS__QUERY);
  console.log("ordersData", ordersData);

  return (
    <>
      <Heading as="h1" size="lg" lineHeight="taller" marginBottom={4}>
        Orders
      </Heading>
      {ordersLoading ? (
        <VStack spacing={3}>
          <Spinner size="md" emptyColor="gray.200" />
          <Text color="gray.500">Loading orders...</Text>
        </VStack>
      ) : (
        <VStack spacing={12} align="stretch">
          <Table variant="simple">
            <Thead>
              <Tr>
                <Th>Order ID</Th>
                <Th>Charge</Th>
                <Th>Date</Th>
                <Th>Orders</Th>
              </Tr>
            </Thead>
            <Tbody>
              {ordersData?.getOrders.map((centollOrder) => (
                <Tr key={`centoll-order-${centollOrder._id}`} fontSize={14}>
                  <Td>{centollOrder._id}</Td>
                  <Td>{centollOrder.charge}</Td>
                  <Td>{moment(centollOrder.date).format("MMMM Do YYYY")}</Td>
                  <Td>
                    <VStack align="start" spacing={2}>
                      {centollOrder.orders.length &&
                        centollOrder.orders.map((order) => (
                          <Text>
                            {order?.totalPriceSet?.presentmentMoney?.amount}{" "}
                            {
                              order?.totalPriceSet?.presentmentMoney
                                ?.currencyCode
                            }
                          </Text>
                        ))}
                    </VStack>
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </VStack>
      )}
    </>
  );
};

export default OrderHistory;
