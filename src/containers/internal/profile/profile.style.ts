import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";

export const ProfileStyle = styled.div`
  margin: 0 auto;

  .profile-hero {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-bottom: 40px;

    &__image {
      width: 148px;
      max-width: 148px;
      height: 148px;
      max-height: 148px;
      border-radius: 100%;
      overflow: hidden;
      margin-bottom: 24px;

      img {
        object-fit: cover;
      }
    }
  }

  .profile-card {
    background-color: ${colors.bn.white};
    border: 1px solid ${colors.bn.gray_200};
    border-radius: 12px;
    padding: 24px;

    &:not(:last-child) {
      margin-bottom: 24px;
    }

    .profile-card-title {
      margin-bottom: 24px;
    }

    .profile-card-paymentMethod {
      &-summary {
        display: flex;
        justify-content: flex-start;
        align-items: center;

        &__brand {
          margin-right: 12px;
          width: 50px;
        }

        &__content {
          flex: 1;
          text-transform: capitalize;
        }
      }

      &-detail {
      }
    }

    .profile-card-delete {
      margin-top: 12px;
    }
  }
`;
