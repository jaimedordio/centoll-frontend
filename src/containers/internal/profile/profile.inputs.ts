import { ERROR_MESSAGES } from "../../../constants/errorMessages";

/* Personal information form */
export const USER_EMAIL_INPUT_NAME = "userEmail";
export const USER_FIRST_NAME_INPUT_NAME = "userFirstName";
export const USER_LAST_NAME_INPUT_NAME = "userLastName";

/* Password change form */
export const OLD_PASSWORD_INPUT_NAME = "oldPassword";
export const NEW_PASSWORD_INPUT_NAME = "newPassword";

/* Address form */
export const ADDRESS_1_INPUT_NAME = "address1";
export const ADDRESS_2_INPUT_NAME = "address2";
export const CITY_INPUT_NAME = "city";
export const COUNTRY_INPUT_NAME = "country";
export const FIRST_NAME_INPUT_NAME = "firstName";
export const LAST_NAME_INPUT_NAME = "lastName";
export const PHONE_INPUT_NAME = "phone";
export const PROVINCE_INPUT_NAME = "province";
export const ZIP_INPUT_NAME = "zip";
export const COUNTRY_CODE_INPUT_NAME = "countryCode";
export const PROVINCE_CODE_INPUT_NAME = "provinceCode";

export const getNewPersonalInformationFormInputMetadata = (
  inputName: string,
  extraValidation: any = {}
): { [key: string]: any } => {
  const inputsArray: any = {
    [USER_EMAIL_INPUT_NAME]: {
      required: { value: true, message: "required" },
    },
    [USER_FIRST_NAME_INPUT_NAME]: {
      required: { value: true, message: "required" },
    },
    [USER_LAST_NAME_INPUT_NAME]: {
      required: { value: true, message: "required" },
    },
  };

  return inputsArray[inputName];
};

export const getNewPasswordFormInputMetadata = (
  inputName: string,
  extraValidation: any = {}
): { [key: string]: any } => {
  const inputsArray: any = {
    [OLD_PASSWORD_INPUT_NAME]: {
      required: { value: true, message: "required" },
    },
    [NEW_PASSWORD_INPUT_NAME]: {
      required: { value: true, message: "required" },
    },
  };

  return inputsArray[inputName];
};

export const getNewShippingFormInputMetadata = (
  inputName: string,
  extraValidation: any = {}
): { [key: string]: any } => {
  const inputsArray: any = {
    [ADDRESS_1_INPUT_NAME]: {
      required: { value: true, message: "required" },
    },
    [ADDRESS_2_INPUT_NAME]: {},
    [CITY_INPUT_NAME]: { required: { value: true, message: "required" } },
    [COUNTRY_INPUT_NAME]: { required: { value: true, message: "required" } },
    [FIRST_NAME_INPUT_NAME]: { required: { value: true, message: "required" } },
    [LAST_NAME_INPUT_NAME]: { required: { value: true, message: "required" } },
    [PHONE_INPUT_NAME]: {},
    [PROVINCE_INPUT_NAME]: { required: { value: true, message: "required" } },
    [ZIP_INPUT_NAME]: { required: { value: true, message: "required" } },
    [COUNTRY_CODE_INPUT_NAME]: {
      required: { value: true, message: "required" },
    },
    [PROVINCE_CODE_INPUT_NAME]: {
      required: { value: true, message: "required" },
    },
  };

  return inputsArray[inputName];
};

export const getNewPersonalInformationFormErrors = (
  inputErrors: any,
  inputName: string
) => {
  if (!inputErrors[inputName]) return;
  if (inputErrors[inputName]?.type === "isValidEmail") return "invalid-email";

  return (
    ERROR_MESSAGES[inputErrors[inputName]?.message] ||
    inputErrors[inputName]?.message
  );
};

export const getNewPasswordFormErrors = (
  inputErrors: any,
  inputName: string
) => {
  if (!inputErrors[inputName]) return;

  return (
    ERROR_MESSAGES[inputErrors[inputName]?.message] ||
    inputErrors[inputName]?.message
  );
};

export const getNewShippingFormErrors = (
  inputErrors: any,
  inputName: string
) => {
  if (!inputErrors[inputName]) return;

  if (inputErrors[inputName]?.type === "isValidEmail") return "invalid-email";
  if (inputErrors[inputName]?.type === "isValidPassword")
    return "invalid-password";
  if (inputErrors[inputName]?.type === "passwordMissmatch") return "no-match";

  return (
    ERROR_MESSAGES[inputErrors[inputName]?.message] ||
    inputErrors[inputName]?.message
  );
};
