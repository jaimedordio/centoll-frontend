import { useMutation, useQuery } from "@apollo/react-hooks";
import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  AspectRatio,
  Badge,
  Button,
  ButtonGroup,
  Center,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  HStack,
  Icon,
  IconButton,
  Image,
  Input,
  Link,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Select,
  SimpleGrid,
  Skeleton,
  SkeletonCircle,
  SkeletonText,
  StackDivider,
  Text,
  useDisclosure,
  VStack,
} from "@chakra-ui/react";
import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js";
import country from "country-state-city";
import gravatar from "gravatar";
import md5 from "md5";
import moment from "moment";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { HiExternalLink } from "react-icons/hi";
import { IoAdd, IoAlert, IoPencil, IoTrash } from "react-icons/io5";
import { images } from "../../../assets/images/images";
import countries from "../../../constants/countries";
import {
  CHANGE_DEFAULT_ADDRESS__MUTATION,
  CHANGE_STRIPE_DEFAULT_SOURCE__MUTATION,
  CREATE_STRIPE_SOURCE__MUTATION,
  DELETE_ADDRESS__MUTATION,
  DELETE_STRIPE_SOURCE__MUTATION,
  NEW_ADDRESS__MUTATION,
  UPDATE_PASSWORD__MUTATION,
  UPDATE_PERSONAL_INFORMATION__MUTATION,
} from "../../../gql/mutations";
import {
  PAYMENT_METHODS__QUERY,
  SHIPPING_ADDRESSES__QUERY,
  USER__QUERY,
} from "../../../gql/queries";
import {
  MailingAddress,
  stripe__PaymentMethod,
  User,
} from "../../../types/gql";
import { notify } from "../../../utils/toast";
import {
  ADDRESS_1_INPUT_NAME,
  ADDRESS_2_INPUT_NAME,
  CITY_INPUT_NAME,
  COUNTRY_INPUT_NAME,
  FIRST_NAME_INPUT_NAME,
  getNewPasswordFormErrors,
  getNewPasswordFormInputMetadata,
  getNewPersonalInformationFormErrors,
  getNewPersonalInformationFormInputMetadata,
  getNewShippingFormErrors,
  getNewShippingFormInputMetadata,
  LAST_NAME_INPUT_NAME,
  NEW_PASSWORD_INPUT_NAME,
  OLD_PASSWORD_INPUT_NAME,
  PROVINCE_CODE_INPUT_NAME,
  USER_EMAIL_INPUT_NAME,
  USER_FIRST_NAME_INPUT_NAME,
  USER_LAST_NAME_INPUT_NAME,
  ZIP_INPUT_NAME,
} from "./profile.inputs";

interface ProfileEditForm {
  userEmail: string;
  userFirstName: string;
  userLastName: string;
}

interface NewPasswordForm {
  oldPassword: string;
  newPassword: string;
}

interface NewShippingAddressForm {
  address1: string;
  address2: string;
  city: string;
  country: string;
  firstName: string;
  lastName: string;
  phone: string;
  province: string;
  zip: string;
  countryCode: string;
  provinceCode: string;
}

const Profile: React.FC = () => {
  const stripe = useStripe();
  const elements = useElements();

  /**
   * ================================
   * Native Hooks
   * ================================
   */
  const [profileEditMode, setProfileEditMode] = useState<boolean>(false);

  /**
   * ================================
   * Modal Hooks
   * ================================
   */
  const {
    isOpen: isNewPasswordModalOpen,
    onOpen: onNewPasswordModalOpen,
    onClose: onNewPasswordModalClose,
  } = useDisclosure();

  const {
    isOpen: isNewAddressModalOpen,
    onOpen: onNewAddressModalOpen,
    onClose: onNewAddressModalClose,
  } = useDisclosure();

  const {
    isOpen: isNewPaymentSourceModalOpen,
    onOpen: onNewPaymentSourceModalOpen,
    onClose: onNewPaymentSourceModalClose,
  } = useDisclosure();

  /**
   * ================================
   * Forms Hooks
   * ================================
   */
  const {
    register: profileFormRegister,
    handleSubmit: profileFormHandleSubmit,
    reset: profileFormHandleReset,
    errors: profileFormErrors,
    formState: { isDirty: profileFormIsDirty },
  } = useForm<ProfileEditForm>({ mode: "onChange" });
  const {
    register: newPasswordFormRegister,
    getValues: newPasswordFormGetValues,
    setValue: newPasswordFormSetValues,
    handleSubmit: newPasswordFormHandleSubmit,
    reset: newPasswordFormHandleReset,
    errors: newPasswordFormErrors,
    formState: { isDirty: newPasswordFormIsDirty },
  } = useForm<NewPasswordForm>({ mode: "onChange" });
  const {
    register: addressFormRegister,
    getValues: addressFormGetValues,
    setValue: addressFormSetValue,
    reset: addressFormResetValues,
    handleSubmit: addressFormHandleSubmit,
    errors: addressFormErrors,
    formState: { isDirty: addressFormIsDirty },
  } = useForm<NewShippingAddressForm>({ mode: "onChange" });

  /**
   * ================================
   * GraphQL Hooks
   * ================================
   */
  /* Queries */
  const { loading: userLoading, data: userData } = useQuery<{
    getUser: User;
  }>(USER__QUERY);
  const {
    data: paymentMethodsData,
    loading: paymentMethodsLoading,
    refetch: getPaymentMethods,
  } = useQuery<{
    getUserPaymentMethods: stripe__PaymentMethod[];
  }>(PAYMENT_METHODS__QUERY, { fetchPolicy: "no-cache" });
  const {
    data: shippingAddressesData,
    loading: shippingAddressesLoading,
    refetch: getShippingAddresses,
  } = useQuery<{
    getUserShippingAddresses: MailingAddress[];
  }>(SHIPPING_ADDRESSES__QUERY, { fetchPolicy: "no-cache" });

  /* Mutations */
  const [updateUserInformation, { loading: updateUserInformationLoading }] =
    useMutation(UPDATE_PERSONAL_INFORMATION__MUTATION);
  const [updatePassword, { loading: updatePasswordLoading }] = useMutation(
    UPDATE_PASSWORD__MUTATION
  );
  const [addShippingAddress, { loading: addShippingAddressLoading }] =
    useMutation(NEW_ADDRESS__MUTATION);
  const [changeDefaultAddress, { loading: changeDefaultAddressLoading }] =
    useMutation(CHANGE_DEFAULT_ADDRESS__MUTATION);
  const [deleteAddress, { loading: deleteAddressLoading }] = useMutation(
    DELETE_ADDRESS__MUTATION
  );
  const [createStripeSource, { loading: createStripeSourceLoading }] =
    useMutation(CREATE_STRIPE_SOURCE__MUTATION);
  const [
    changeStripeDefaultSource,
    { loading: changeStripeDefaultSourceLoading },
  ] = useMutation(CHANGE_STRIPE_DEFAULT_SOURCE__MUTATION);
  const [deleteStripeSource, { loading: deleteStripeSourceLoading }] =
    useMutation(DELETE_STRIPE_SOURCE__MUTATION);

  /**
   * ================================
   * Custom methods
   * ================================
   */
  const onUpdatePersonalInformation = (formData: ProfileEditForm) => {
    console.log("formData", formData);
    updateUserInformation({
      variables: {
        newInfo: {
          email: formData.userEmail,
          firstName: formData.userFirstName,
          lastName: formData.userLastName,
        },
      },
    })
      .then(() => {
        notify({ type: "success", message: "Personal information updated!" });
        setProfileEditMode(false);
      })
      .catch((err) => {
        notify({ type: "error", message: "Error updating information" });
      });
  };

  const onNewPassword = (formData: NewPasswordForm) => {
    console.log("formData", formData);
    updatePassword({
      variables: {
        oldPassword: md5(formData.oldPassword),
        newPassword: md5(formData.newPassword),
      },
    })
      .then(() => {
        notify({ type: "success", message: "Password updated!" });
        onNewPasswordModalClose();
      })
      .catch((err) => {
        notify({ type: "error", message: `Error updating password: ${err}` });
      });
  };

  const onPaymentSetAsDefault = (sourceId: string) => {
    changeStripeDefaultSource({
      variables: { sourceId },
    }).then(() => {
      notify({ type: "success", message: "Default payment method updated!" });
      getPaymentMethods();
    });
  };

  const onPaymentDelete = (sourceId: string) => {
    deleteStripeSource({
      variables: { sourceId },
    }).then(() => {
      notify({ type: "success", message: "Payment method removed!" });
      getPaymentMethods();
    });
  };

  const onNewShippingAddressSubmit = (formData: NewShippingAddressForm) => {
    console.log("formData", formData);

    addShippingAddress({ variables: { shippingAddress: formData } })
      .then((res) => {
        onNewAddressModalClose();
        notify({ type: "success", message: "Address added to your account!" });
        getShippingAddresses();
      })
      .catch((err) => notify({ message: err, type: "error" }));
  };

  const onDefaultShippingAddressChange = (id: string) => {
    changeDefaultAddress({
      variables: { id },
    })
      .then((res) => {
        notify({ type: "success", message: "Default address updated!" });
        getShippingAddresses();
      })
      .catch((err) => notify({ message: err, type: "error" }));
  };

  const onShippingAddressDelete = (id: string) => {
    deleteAddress({
      variables: { shippingAddress: id },
    })
      .then((res) => {
        notify({ type: "success", message: "Address deleted!" });
        getShippingAddresses();
      })
      .catch((err) => notify({ message: `${err}`, type: "error" }));
  };

  const onNewPaymentSource = async (event: any) => {
    event.preventDefault();

    if (!stripe || !elements) {
      return;
    }

    const cardElement = elements.getElement(CardElement)!;

    const { source, error } = await stripe.createSource(cardElement, {
      type: "card",
    });

    if (error) {
      console.log("[error]", error);
    } else {
      console.log("[Source]", source);

      createStripeSource({
        variables: { sourceId: source?.id },
      })
        .then(() => {
          notify({ type: "success", message: "Payment method added!" });
          getPaymentMethods();
          onNewPaymentSourceModalClose();
        })
        .catch((err) =>
          notify({
            type: "error",
            message: `Error creating payment method: ${err}`,
          })
        );
    }
  };

  return (
    <>
      {userLoading ? (
        <VStack spacing={20} align="center">
          <VStack spacing={10} align="center">
            <SkeletonCircle size="150" />
            <SkeletonText noOfLines={1} minWidth="xs" />
          </VStack>
          <VStack spacing={10} align="center" minWidth="100%">
            <Skeleton height="200px" width="100%" />
          </VStack>
        </VStack>
      ) : (
        <VStack spacing={20} align="strech" py={12}>
          <VStack spacing={10} align="center">
            <AspectRatio
              alignItems="center"
              width="148px"
              ratio={1 / 1}
              borderRadius={100}
              overflow="hidden"
            >
              <Image
                src={
                  localStorage.getItem("_centoll_email")! &&
                  gravatar.url(localStorage.getItem("_centoll_email")!, {
                    s: "200",
                  })
                }
                alt=""
                objectFit="cover"
              />
            </AspectRatio>
            <VStack spacing={1} align="center">
              <Text fontSize="smaller" color="gray.500">
                Profile image from{" "}
                <Link
                  textDecoration="underline"
                  href={`https://gravatar.com/`}
                  isExternal
                >
                  Gravatar <Icon as={HiExternalLink} mx="2px" />
                </Link>
              </Text>
              <Text fontSize="sm" color="gray.500">
                Joined{" "}
                {userData &&
                  moment(userData.getUser.userSince, "DD/MM/YYYY").fromNow()}
              </Text>
            </VStack>
          </VStack>
          <VStack align="strech" spacing={4}>
            <HStack
              justifyContent="space-between"
              spacing={2}
              paddingBottom={2}
              borderBottom="1px"
              borderColor="gray.200"
            >
              <Heading as="h2" size="md">
                Personal information
              </Heading>
              {!profileEditMode ? (
                <Button
                  colorScheme="gray"
                  size="sm"
                  leftIcon={<IoPencil />}
                  onClick={() => setProfileEditMode(true)}
                >
                  Edit
                </Button>
              ) : (
                <HStack align="flex-end">
                  <Button
                    colorScheme="gray"
                    size="sm"
                    onClick={() => {
                      profileFormHandleReset({
                        userEmail: userData?.getUser.email,
                        userFirstName: userData?.getUser.firstName,
                        userLastName: userData?.getUser.lastName,
                      });
                      setProfileEditMode(false);
                    }}
                  >
                    Cancel
                  </Button>
                  <Button
                    colorScheme="blue"
                    size="sm"
                    isLoading={updateUserInformationLoading}
                    isDisabled={!profileFormIsDirty}
                    onClick={profileFormHandleSubmit(
                      onUpdatePersonalInformation
                    )}
                  >
                    Save
                  </Button>
                </HStack>
              )}
            </HStack>
            {userData?.getUser ? (
              <VStack spacing={6}>
                <FormControl
                  id="email"
                  isInvalid={Boolean(profileFormErrors[USER_EMAIL_INPUT_NAME])}
                >
                  <HStack
                    spacing={5}
                    justifyContent="start"
                    alignItems="center"
                  >
                    <FormLabel color="gray.500" fontSize="sm" minWidth="sm">
                      Email
                    </FormLabel>
                    <Input
                      isReadOnly={!profileEditMode}
                      variant={!profileEditMode ? "unstyled" : "outline"}
                      maxWidth="md"
                      size="sm"
                      defaultValue={userData?.getUser.email}
                      name={USER_EMAIL_INPUT_NAME}
                      ref={profileFormRegister(
                        getNewPersonalInformationFormInputMetadata(
                          USER_EMAIL_INPUT_NAME
                        )
                      )}
                    />
                    <FormErrorMessage>
                      {getNewPersonalInformationFormErrors(
                        profileFormErrors,
                        USER_EMAIL_INPUT_NAME
                      )}
                    </FormErrorMessage>
                  </HStack>
                </FormControl>
                <FormControl id="firstName">
                  <HStack
                    spacing={5}
                    justifyContent="start"
                    alignItems="center"
                  >
                    <FormLabel color="gray.500" fontSize="sm" minWidth="sm">
                      First name
                    </FormLabel>
                    <Input
                      isReadOnly={!profileEditMode}
                      variant={!profileEditMode ? "unstyled" : "outline"}
                      maxWidth="md"
                      size="sm"
                      defaultValue={userData?.getUser.firstName}
                      name={USER_FIRST_NAME_INPUT_NAME}
                      ref={profileFormRegister(
                        getNewPersonalInformationFormInputMetadata(
                          USER_FIRST_NAME_INPUT_NAME
                        )
                      )}
                    />
                    <FormErrorMessage>
                      {getNewPersonalInformationFormErrors(
                        profileFormErrors,
                        USER_FIRST_NAME_INPUT_NAME
                      )}
                    </FormErrorMessage>
                  </HStack>
                </FormControl>
                <FormControl id="lastName">
                  <HStack
                    spacing={5}
                    justifyContent="start"
                    alignItems="flex-start"
                  >
                    <FormLabel color="gray.500" fontSize="sm" minWidth="sm">
                      Last name
                    </FormLabel>
                    <Input
                      isReadOnly={!profileEditMode}
                      variant={!profileEditMode ? "unstyled" : "outline"}
                      maxWidth="md"
                      size="sm"
                      defaultValue={userData?.getUser.lastName}
                      name={USER_LAST_NAME_INPUT_NAME}
                      ref={profileFormRegister(
                        getNewPersonalInformationFormInputMetadata(
                          USER_LAST_NAME_INPUT_NAME
                        )
                      )}
                    />
                    <FormErrorMessage>
                      {getNewPersonalInformationFormErrors(
                        profileFormErrors,
                        USER_LAST_NAME_INPUT_NAME
                      )}
                    </FormErrorMessage>
                  </HStack>
                </FormControl>
                <FormControl id="password">
                  <HStack
                    spacing={5}
                    justifyContent="start"
                    alignItems="flex-start"
                  >
                    <FormLabel color="gray.500" fontSize="sm" minWidth="sm">
                      Password
                    </FormLabel>
                    <Button
                      colorScheme="blue"
                      size="xs"
                      onClick={() => onNewPasswordModalOpen()}
                    >
                      Change password
                    </Button>
                  </HStack>
                </FormControl>
              </VStack>
            ) : (
              ""
            )}
          </VStack>
          <VStack align="strech" spacing={4}>
            <HStack
              justifyContent="space-between"
              spacing={2}
              paddingBottom={2}
              borderBottom="1px"
              borderColor="gray.200"
            >
              <Heading as="h2" size="md">
                Addresses
              </Heading>
              <Button
                colorScheme="gray"
                size="sm"
                leftIcon={<IoAdd />}
                onClick={onNewAddressModalOpen}
              >
                New
              </Button>
            </HStack>
            {shippingAddressesLoading ? (
              <>
                <VStack
                  align="stretch"
                  spacing={3}
                  divider={<StackDivider borderBottom="1px" color="gray.200" />}
                >
                  <SkeletonText noOfLines={3} />
                </VStack>
                <VStack
                  align="stretch"
                  spacing={3}
                  divider={<StackDivider borderBottom="1px" color="gray.200" />}
                >
                  <SkeletonText noOfLines={3} />
                </VStack>
              </>
            ) : shippingAddressesData ? (
              <SimpleGrid spacing={6} columns={3}>
                {shippingAddressesData.getUserShippingAddresses.map(
                  (shippingAddress, shippingAddressIndex) => (
                    <VStack
                      key={`shipping-${shippingAddressIndex}`}
                      align="stretch"
                      justifyContent="space-between"
                      spacing={3}
                      p={4}
                      minW="xs"
                      borderWidth="1px"
                      borderStyle="solid"
                      borderColor="gray.200"
                      borderRadius="md"
                    >
                      <VStack align="stretch" spacing={2}>
                        {shippingAddress.default && (
                          <Badge
                            width="fit-content"
                            colorScheme="blue"
                            borderRadius="md"
                          >
                            Default
                          </Badge>
                        )}
                        <VStack align="stretch" spacing={0}>
                          <Text fontSize="sm" fontWeight={500}>
                            {`${shippingAddress.firstName} ${shippingAddress.lastName}`}
                          </Text>
                          <Text fontSize="sm">
                            {`${shippingAddress.address1}, ${shippingAddress.address2}`}
                          </Text>
                          <Text fontSize="sm">
                            {`${shippingAddress.zip}, ${
                              shippingAddress.city
                            }, ${
                              country.getCountryByCode(shippingAddress.country!)
                                .name
                            }`}
                          </Text>
                        </VStack>
                      </VStack>
                      <ButtonGroup
                        alignSelf="stretch"
                        justifyContent="space-between"
                        size="sm"
                        variant="link"
                        spacing={4}
                      >
                        <HStack spacing={4}>
                          {!shippingAddress.default && (
                            <Button
                              fontWeight="400"
                              colorScheme="blue"
                              isLoading={changeDefaultAddressLoading}
                              onClick={() =>
                                onDefaultShippingAddressChange(
                                  shippingAddress._id
                                )
                              }
                            >
                              Set as default
                            </Button>
                          )}
                        </HStack>
                        <HStack spacing={4}>
                          <Button
                            fontWeight="400"
                            colorScheme="red"
                            onClick={() =>
                              onShippingAddressDelete(shippingAddress._id)
                            }
                          >
                            Delete
                          </Button>
                        </HStack>
                      </ButtonGroup>
                    </VStack>
                  )
                )}
              </SimpleGrid>
            ) : (
              <Alert
                status="info"
                variant="subtle"
                flexDirection="column"
                alignItems="center"
                justifyContent="center"
                textAlign="center"
                height="200px"
                colorScheme="gray"
              >
                <AlertIcon boxSize="40px" mr={0} />
                <AlertTitle mt={4} mb={1} fontSize="lg">
                  No shipping addresses added
                </AlertTitle>
                <AlertDescription maxWidth="sm">
                  Add your first shipping address with the 'New' button
                </AlertDescription>
              </Alert>
            )}
          </VStack>
          <VStack align="strech" spacing={4}>
            <HStack
              justifyContent="space-between"
              spacing={2}
              paddingBottom={2}
              borderBottom="1px"
              borderColor="gray.200"
            >
              <Heading as="h2" size="md">
                Payment methods
              </Heading>
              <Button
                colorScheme="gray"
                size="sm"
                leftIcon={<IoAdd />}
                onClick={onNewPaymentSourceModalOpen}
              >
                New
              </Button>
            </HStack>
            <VStack
              align="stretch"
              spacing={3}
              divider={<StackDivider borderBottom="1px" color="gray.200" />}
            >
              {paymentMethodsLoading ? (
                <VStack
                  align="stretch"
                  spacing={3}
                  divider={<StackDivider borderBottom="1px" color="gray.200" />}
                >
                  <HStack spacing={3}>
                    <Skeleton width="48px" height="32px" />
                    <VStack spacing={2} align="flex-start">
                      <SkeletonText noOfLines={1} width="128px" />
                      <SkeletonText noOfLines={1} width="84px" />
                    </VStack>
                  </HStack>
                  <HStack spacing={3}>
                    <Skeleton width="48px" height="32px" />
                    <VStack spacing={2} align="flex-start">
                      <SkeletonText noOfLines={1} width="128px" />
                      <SkeletonText noOfLines={1} width="84px" />
                    </VStack>
                  </HStack>
                </VStack>
              ) : paymentMethodsData?.getUserPaymentMethods.length ? (
                paymentMethodsData.getUserPaymentMethods.map(
                  (paymentMethod, paymentMethodIndex: number) => (
                    <HStack
                      justifyContent="space-between"
                      key={`payment-${paymentMethodIndex}`}
                    >
                      <HStack spacing={3}>
                        <Image
                          width="48px"
                          src={
                            images.paymentMethods[paymentMethod.card?.brand!]
                          }
                          alt={
                            images.paymentMethods[paymentMethod.card?.brand!]
                          }
                        />
                        <VStack spacing={0} align="flex-start">
                          <HStack spacing={2}>
                            <Text fontSize="sm" fontWeight={500}>
                              <span style={{ textTransform: "capitalize" }}>
                                {paymentMethod.card?.brand}
                              </span>{" "}
                              card ending in {paymentMethod.card?.last4}
                            </Text>
                            {paymentMethod.default && (
                              <Badge
                                width="fit-content"
                                colorScheme="blue"
                                borderRadius="md"
                              >
                                Default
                              </Badge>
                            )}
                          </HStack>
                          <Text fontSize="sm" color="gray.500">
                            Expires {paymentMethod.card?.exp_month}/
                            {paymentMethod.card?.exp_year}
                          </Text>
                        </VStack>
                      </HStack>
                      <HStack>
                        <ButtonGroup size="sm" isAttached variant="outline">
                          {!paymentMethod.default && (
                            <Button
                              mr="-px"
                              onClick={() =>
                                onPaymentSetAsDefault(paymentMethod.id)
                              }
                              isLoading={changeStripeDefaultSourceLoading}
                            >
                              Set as default
                            </Button>
                          )}
                          <IconButton
                            aria-label="Delete card"
                            icon={<IoTrash />}
                            isDisabled={
                              paymentMethod.default ||
                              paymentMethodsData.getUserPaymentMethods.length <
                                2
                            }
                            onClick={() => onPaymentDelete(paymentMethod.id)}
                            isLoading={deleteStripeSourceLoading}
                          />
                        </ButtonGroup>
                      </HStack>
                    </HStack>
                  )
                )
              ) : (
                <HStack spacing={4}>
                  <Center
                    backgroundColor="red.200"
                    p={2}
                    borderRadius="full"
                    width="30px"
                    height="30px"
                  >
                    <Icon color="red.500" as={IoAlert} />
                  </Center>
                  <Text color="gray.500">No payment methods added</Text>
                </HStack>
              )}
            </VStack>
          </VStack>
          {/* <VStack align="strech" spacing={4}>
            <HStack
              justifyContent="space-between"
              spacing={2}
              paddingBottom={2}
              borderBottom="1px"
              borderColor="gray.200"
            >
              <Heading as="h2" size="md">
                Danger zone
              </Heading>
            </HStack>
            <VStack align="flex-start" spacing={3}>
              <Text>
                If you delete your account, all data related to your profile
                will be
                <b> permanently</b> deleted. There's no way back.
              </Text>
              <Button colorScheme="red" variant="link" size="md">
                Delete my account
              </Button>
            </VStack>
          </VStack> */}
        </VStack>
      )}
      <Modal
        isOpen={isNewPasswordModalOpen}
        onClose={onNewPasswordModalClose}
        size="lg"
        isCentered
      >
        <ModalOverlay />
        <ModalContent pb={2}>
          <ModalHeader>Update password</ModalHeader>
          <ModalCloseButton top={4} right={6} />
          <ModalBody>
            <VStack align="stretch" spacing={6}>
              <FormControl
                isInvalid={Boolean(
                  newPasswordFormErrors[OLD_PASSWORD_INPUT_NAME]
                )}
              >
                <Input
                  type="password"
                  size="md"
                  fontSize="sm"
                  borderRadius="md"
                  name={OLD_PASSWORD_INPUT_NAME}
                  placeholder="Old password"
                  ref={newPasswordFormRegister(
                    getNewPasswordFormInputMetadata(OLD_PASSWORD_INPUT_NAME)
                  )}
                />
                <FormErrorMessage>
                  {getNewPasswordFormErrors(
                    newPasswordFormErrors,
                    OLD_PASSWORD_INPUT_NAME
                  )}
                </FormErrorMessage>
              </FormControl>
              <FormControl
                isInvalid={Boolean(
                  newPasswordFormErrors[NEW_PASSWORD_INPUT_NAME]
                )}
              >
                <Input
                  type="password"
                  size="md"
                  fontSize="sm"
                  borderRadius="md"
                  name={NEW_PASSWORD_INPUT_NAME}
                  placeholder="New password"
                  ref={newPasswordFormRegister(
                    getNewPasswordFormInputMetadata(NEW_PASSWORD_INPUT_NAME)
                  )}
                />
                <FormErrorMessage>
                  {getNewPasswordFormErrors(
                    newPasswordFormErrors,
                    NEW_PASSWORD_INPUT_NAME
                  )}
                </FormErrorMessage>
              </FormControl>
              <Button
                type="submit"
                colorScheme="blue"
                isLoading={updatePasswordLoading}
                isDisabled={!newPasswordFormIsDirty}
                onClick={newPasswordFormHandleSubmit(onNewPassword)}
              >
                Update password
              </Button>
            </VStack>
          </ModalBody>
        </ModalContent>
      </Modal>
      <Modal
        isOpen={isNewAddressModalOpen}
        onClose={onNewAddressModalClose}
        size="lg"
        isCentered
      >
        <ModalOverlay />
        <ModalContent pb={2}>
          <ModalHeader>New shipping address</ModalHeader>
          <ModalCloseButton top={4} right={6} />
          <ModalBody>
            <VStack align="stretch" spacing={6}>
              <VStack align="stretch" spacing={4}>
                <SimpleGrid columns={2} spacingX={4}>
                  <FormControl
                    isInvalid={Boolean(
                      addressFormErrors[FIRST_NAME_INPUT_NAME]
                    )}
                  >
                    <Input
                      size="md"
                      fontSize="sm"
                      borderRadius="md"
                      name={FIRST_NAME_INPUT_NAME}
                      placeholder="First name"
                      ref={addressFormRegister(
                        getNewShippingFormInputMetadata(FIRST_NAME_INPUT_NAME)
                      )}
                    />
                    <FormErrorMessage>
                      {getNewShippingFormErrors(
                        addressFormErrors,
                        FIRST_NAME_INPUT_NAME
                      )}
                    </FormErrorMessage>
                  </FormControl>
                  <FormControl
                    isInvalid={Boolean(addressFormErrors[LAST_NAME_INPUT_NAME])}
                  >
                    <Input
                      size="md"
                      fontSize="sm"
                      borderRadius="md"
                      name={LAST_NAME_INPUT_NAME}
                      placeholder="Last name"
                      ref={addressFormRegister(
                        getNewShippingFormInputMetadata(LAST_NAME_INPUT_NAME)
                      )}
                    />
                    <FormErrorMessage>
                      {getNewShippingFormErrors(
                        addressFormErrors,
                        LAST_NAME_INPUT_NAME
                      )}
                    </FormErrorMessage>
                  </FormControl>
                </SimpleGrid>
                <FormControl
                  isInvalid={Boolean(addressFormErrors[ADDRESS_1_INPUT_NAME])}
                >
                  <Input
                    size="md"
                    fontSize="sm"
                    borderRadius="md"
                    name={ADDRESS_1_INPUT_NAME}
                    placeholder="Street and house number"
                    ref={addressFormRegister(
                      getNewShippingFormInputMetadata(ADDRESS_1_INPUT_NAME)
                    )}
                  />
                  <FormErrorMessage>
                    {getNewShippingFormErrors(
                      addressFormErrors,
                      ADDRESS_1_INPUT_NAME
                    )}
                  </FormErrorMessage>
                </FormControl>
                <FormControl
                  isInvalid={Boolean(addressFormErrors[ADDRESS_2_INPUT_NAME])}
                >
                  <Input
                    size="md"
                    fontSize="sm"
                    borderRadius="md"
                    name={ADDRESS_2_INPUT_NAME}
                    placeholder="Apartment, suite, etc. (optional)"
                    ref={addressFormRegister(
                      getNewShippingFormInputMetadata(ADDRESS_2_INPUT_NAME)
                    )}
                  />
                  <FormErrorMessage>
                    {getNewShippingFormErrors(
                      addressFormErrors,
                      ADDRESS_2_INPUT_NAME
                    )}
                  </FormErrorMessage>
                </FormControl>
                <SimpleGrid columns={3} spacingX={4}>
                  <FormControl
                    isInvalid={Boolean(addressFormErrors[ZIP_INPUT_NAME])}
                  >
                    <Input
                      size="md"
                      fontSize="sm"
                      borderRadius="md"
                      name={ZIP_INPUT_NAME}
                      placeholder="Postal code"
                      ref={addressFormRegister(
                        getNewShippingFormInputMetadata(ZIP_INPUT_NAME)
                      )}
                    />
                    <FormErrorMessage>
                      {getNewShippingFormErrors(
                        addressFormErrors,
                        ZIP_INPUT_NAME
                      )}
                    </FormErrorMessage>
                  </FormControl>
                  <FormControl
                    isInvalid={Boolean(addressFormErrors[CITY_INPUT_NAME])}
                  >
                    <Input
                      size="md"
                      fontSize="sm"
                      borderRadius="md"
                      name={CITY_INPUT_NAME}
                      placeholder="City"
                      ref={addressFormRegister(
                        getNewShippingFormInputMetadata(CITY_INPUT_NAME)
                      )}
                    />
                    <FormErrorMessage>
                      {getNewShippingFormErrors(
                        addressFormErrors,
                        CITY_INPUT_NAME
                      )}
                    </FormErrorMessage>
                  </FormControl>
                  <FormControl
                    isInvalid={Boolean(
                      addressFormErrors[PROVINCE_CODE_INPUT_NAME]
                    )}
                  >
                    <Select
                      size="md"
                      fontSize="sm"
                      borderRadius="md"
                      isFullWidth
                      name={PROVINCE_CODE_INPUT_NAME}
                      placeholder="Province"
                      ref={addressFormRegister(
                        getNewShippingFormInputMetadata(
                          PROVINCE_CODE_INPUT_NAME
                        )
                      )}
                      isDisabled={
                        !Boolean(addressFormGetValues(COUNTRY_INPUT_NAME))
                      }
                    >
                      {addressFormGetValues(COUNTRY_INPUT_NAME) &&
                        country
                          .getStatesOfCountry(
                            addressFormGetValues(COUNTRY_INPUT_NAME)
                          )
                          ?.map((state) => (
                            <option
                              value={state.isoCode}
                              key={`state-${state.isoCode}`}
                            >
                              {state.name}
                            </option>
                          ))}
                    </Select>
                    <FormErrorMessage>
                      {getNewShippingFormErrors(
                        addressFormErrors,
                        PROVINCE_CODE_INPUT_NAME
                      )}
                    </FormErrorMessage>
                  </FormControl>
                </SimpleGrid>
                <FormControl
                  isInvalid={Boolean(addressFormErrors[COUNTRY_INPUT_NAME])}
                >
                  <Select
                    size="md"
                    fontSize="sm"
                    borderRadius="md"
                    isFullWidth
                    name={COUNTRY_INPUT_NAME}
                    placeholder="Country"
                    ref={addressFormRegister(
                      getNewShippingFormInputMetadata(COUNTRY_INPUT_NAME)
                    )}
                  >
                    {countries.map((country) => (
                      <option
                        value={country.abbreviation}
                        key={`country-${country.abbreviation}`}
                      >
                        {country.country}
                      </option>
                    ))}
                  </Select>
                  <FormErrorMessage>
                    {getNewShippingFormErrors(
                      addressFormErrors,
                      COUNTRY_INPUT_NAME
                    )}
                  </FormErrorMessage>
                </FormControl>
              </VStack>
              <Button
                colorScheme="blue"
                size="md"
                isDisabled={!addressFormIsDirty}
                isLoading={addShippingAddressLoading}
                onClick={addressFormHandleSubmit(onNewShippingAddressSubmit)}
              >
                Save address
              </Button>
            </VStack>
          </ModalBody>
        </ModalContent>
      </Modal>
      <Modal
        isOpen={isNewPaymentSourceModalOpen}
        onClose={onNewPaymentSourceModalClose}
        size="lg"
        isCentered
      >
        <ModalOverlay />
        <ModalContent pb={2}>
          <ModalHeader>New payment method</ModalHeader>
          <ModalCloseButton top={4} right={6} />
          <ModalBody>
            <form onSubmit={onNewPaymentSource}>
              <VStack align="stretch" spacing={6}>
                <CardElement />
                <Button
                  type="submit"
                  colorScheme="blue"
                  size="sm"
                  isDisabled={!stripe}
                  isLoading={createStripeSourceLoading}
                >
                  Save card
                </Button>
              </VStack>
            </form>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default Profile;
