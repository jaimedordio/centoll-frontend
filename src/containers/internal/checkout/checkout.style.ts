import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";

export const CheckoutStyle = styled.div`
  .checkout-body {
    display: grid;
    grid-template-columns: 4fr 2fr;

    .checkout-body-sections {
      flex: 1;

      .checkout-body-sections-card {
        background-color: ${colors.bn.white};
        border: 1px solid ${colors.bn.gray_200};
        border-radius: 12px;
        padding: 16px;

        &:not(:last-child) {
          margin-bottom: 16px;
        }

        .checkout-body-sections-card-heading {
          display: flex;
          justify-content: space-between;
          align-items: center;
          margin-bottom: 12px;

          &__text {
            margin-top: 0;
          }
        }

        .checkout-body-sections-card-content {
        }

        .section-address {
          .section-address-item {
            &:not(:last-child) {
              margin-bottom: 14px;
            }

            .section-address-item-content {
              display: flex;
              justify-content: flex-start;
              align-items: center;

              &__tag {
                margin-left: 8px;
              }
            }
          }
        }

        .section-payment {
          .section-payment-item {
            display: flex;
            justify-content: flex-start;
            align-items: center;

            &__brand {
              margin-right: 12px;
              width: 50px;
            }

            &__content {
              flex: 1;
              text-transform: capitalize;
            }
            &:not(:last-child) {
              margin-bottom: 14px;
            }

            .section-payment-item-content {
              display: flex;
              justify-content: flex-start;
              align-items: center;

              &__tag {
                margin-left: 8px;
              }
            }
          }
        }

        .section-products {
          .section-products-shop {
            &:not(:last-child) {
              border-bottom: 1px solid ${colors.bn.gray_200};
              margin-bottom: 16px;
            }

            .section-products-shop-heading {
              display: flex;
              justify-content: flex-start;
              align-items: center;
            }

            .section-products-shop-delivery {
            }

            .section-products-shop-items {
              display: flex;
              align-items: flex-start;

              &:not(:last-child) {
                margin-bottom: 8px;
              }

              .section-products-shop-items-col {
                width: 50%;

                .section-products-shop-items-col-item {
                  display: flex;
                  justify-content: flex-start;
                  align-items: center;
                  padding: 4px 0;

                  &__img {
                    background-color: ${colors.bn.gray_200};
                    min-width: 50px;
                    max-width: 50px;
                    min-height: 50px;
                    max-height: 50px;
                    margin-right: 16px;
                    border-radius: 8px;
                    overflow: hidden;
                    display: flex;
                    justify-content: center;
                    align-items: center;

                    img {
                      object-fit: cover;
                      height: 50px;
                    }
                  }
                }

                .section-products-shop-items-col-shipping {
                  &:not(:last-child) {
                    margin-bottom: 4px;
                  }
                }
              }
            }
          }
        }
      }
    }

    .checkout-body-confirm {
      background-color: ${colors.bn.white};
      border: 1px solid ${colors.bn.gray_200};
      border-radius: 12px;
      padding: 24px;
      margin-left: 24px;
      min-width: 25%;
      height: fit-content;

      .checkout-body-confirm-title {
        margin-bottom: 12px;
      }

      .checkout-body-confirm-content,
      .checkout-body-confirm-total {
        &__value {
          text-align: end;
        }
      }

      .checkout-body-confirm-content {
        display: grid;
        grid-template-columns: 1fr max-content;
        gap: 4px 12px;
        padding: 8px 0;
      }

      .checkout-body-confirm-total {
        border-top: 1px solid ${colors.bn.gray_200};
        display: grid;
        grid-template-columns: 1fr max-content;
        gap: 8px 12px;
        padding: 8px 0;
      }

      .checkout-body-confirm-action {
        margin-top: 12px;
      }
    }
  }
`;
