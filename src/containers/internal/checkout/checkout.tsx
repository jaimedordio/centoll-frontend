import { useMutation, useQuery } from "@apollo/react-hooks";
import {
  AspectRatio,
  Badge,
  Button as ChakraButton,
  Center,
  Checkbox,
  Divider,
  Grid,
  Heading,
  HStack,
  Icon,
  Image,
  Link,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  SimpleGrid,
  Spinner,
  Stack,
  StackDivider,
  Text,
  useDisclosure,
  VStack,
} from "@chakra-ui/react";
import React, { useEffect, useMemo, useState } from "react";
import { HiExternalLink } from "react-icons/hi";
import { IoIosCart, IoIosPricetag } from "react-icons/io";
import {
  IoAlert,
  IoAlertCircleOutline,
  IoBan,
  IoPencil,
} from "react-icons/io5";
import { useHistory } from "react-router-dom";
import { images } from "../../../assets/images/images";
import { COMPLETE_CHECKOUT__MUTATION } from "../../../gql/mutations";
import {
  GROUPED_CART__QUERY,
  PAYMENT_METHODS__QUERY,
  USER__QUERY,
} from "../../../gql/queries";
import {
  CalculatedDraftOrder,
  CompleteShopCheckoutInput,
  GroupedCart,
  MailingAddress,
  ShippingRate,
  stripe__PaymentMethod,
  User,
} from "../../../types/gql";
import { apolloFetch } from "../../../utils/apollo-client";
import CurrencyNumber from "../../../utils/currencyNumber";
import { getFavicons } from "../../../utils/functions";
import { notify } from "../../../utils/toast";
import { CheckoutStyle } from "./checkout.style";

const Checkout: React.FC = () => {
  /**
   * ================================
   * Main Hooks
   * ================================
   */
  const history = useHistory();

  const {
    isOpen: isShippingModalOpen,
    onOpen: onShippingModalOpen,
    onClose: onShippingModalClose,
  } = useDisclosure();
  const {
    isOpen: isPaymentModalOpen,
    onOpen: onPaymentModalOpen,
    onClose: onPaymentModalClose,
  } = useDisclosure();

  /**
   * ================================
   * GraphQL Hooks
   * ================================
   */

  /* Queries */
  const { loading: userLoading, data: userData } = useQuery<{
    getUser: User;
  }>(USER__QUERY);
  const {
    error: groupedCartartError,
    loading: groupedCartartLoading,
    data: groupedCartartData,
  } = useQuery<{
    getGroupedCart: GroupedCart[];
  }>(GROUPED_CART__QUERY);
  const {
    data: paymentMethodsData,
    loading: paymentMethodsLoading,
  } = useQuery<{
    getUserPaymentMethods: stripe__PaymentMethod[];
  }>(PAYMENT_METHODS__QUERY);

  /* Mutations */
  const [completeCheckout, { loading: completeCheckoutLoading }] = useMutation(
    COMPLETE_CHECKOUT__MUTATION
  );

  /**
   * ================================
   * Native Hooks
   * ================================
   */
  const [addressState, setAddressState] = useState<MailingAddress>();
  const [subtotalState, setSubtotalState] = useState<number>(0);
  const [taxesState, setTaxesState] = useState<number>(0);
  const [shippingCostState, setShippingCostState] = useState<number>(0);
  const [shippingsState, setShippingsState] = useState<{
    [shop: string]: ShippingRate[];
  }>({});
  const [
    selectedCardState,
    setSelectedCardState,
  ] = useState<stripe__PaymentMethod>();
  const [checkedShippings, setCheckedShippings] = useState<{
    [shop: string]: ShippingRate;
  }>({});
  const [faviconState, setFaviconState] = useState<{ [key: string]: string }>(
    {}
  );

  const domains = useMemo(() => {
    if (groupedCartartData?.getGroupedCart) {
      return groupedCartartData?.getGroupedCart
        .map((cart) => cart.shop.myshopifyDomain)
        .filter((elem, index, self) => index === self.indexOf(elem));
    }
    return [];
  }, [groupedCartartData]);

  /**
   * ================================
   * Effects
   * ================================
   */
  useEffect(() => {
    if (groupedCartartError) {
      console.error(`User cart error: ${groupedCartartError}`);
      notify({
        message: `User cart error: ${groupedCartartError}`,
        type: "error",
      });
    }
  }, [groupedCartartError]);

  useEffect(() => {
    if (userData) {
      setAddressState(
        userData.getUser.shippingAddresses?.find((address) => address.default)
      );
    }
  }, [userData]);

  useEffect(() => {
    if (paymentMethodsData) {
      setSelectedCardState(
        paymentMethodsData.getUserPaymentMethods.find(
          (paymentMethod) => paymentMethod.default
        )
      );
    }
  }, [paymentMethodsData]);

  useEffect(() => {
    if (groupedCartartData?.getGroupedCart && addressState) {
      console.log(
        "groupedCartartData.getGroupedCart",
        groupedCartartData.getGroupedCart
      );

      const calculated_draft_orders_promises: Promise<{
        draftOrderCalculate: CalculatedDraftOrder;
      }>[] = [];

      const newShippingsState: { [shop: string]: ShippingRate[] } = {};
      const newCheckedShippings: { [shop: string]: ShippingRate } = {};
      const newShippingCostState: number[] = [];

      // Delete default property from address object for matching MailingAddressInput type
      const cleanAddress = JSON.parse(JSON.stringify(addressState));
      delete cleanAddress[`default`];
      delete cleanAddress[`_id`];

      for (const cart of groupedCartartData.getGroupedCart) {
        const gql_items = cart.items?.map((item) => ({
          variantId: item.id,
          quantity: item.quantity,
        }));

        calculated_draft_orders_promises.push(
          apolloFetch({
            query: `
              query($shop: String!, $input: DraftOrderInput!) {
                draftOrderCalculate(shop: $shop, input: $input) {
                  subtotalPrice
                  totalPrice
                  totalShippingPrice
                  totalTax
                  availableShippingRates {
                    title
                    handle
                    price {
                      amount
                      currencyCode
                    }
                  }
                }
              }
            `,
            variables: {
              shop: cart.shop.myshopifyDomain,
              input: {
                lineItems: gql_items,
                shippingAddress: cleanAddress,
              },
            },
          })
            .then(({ data }) => {
              if (data) {
                const availableShippingRates: ShippingRate[] =
                  data.draftOrderCalculate.availableShippingRates;

                if (availableShippingRates) {
                  newShippingsState[
                    cart.shop.myshopifyDomain
                  ] = availableShippingRates;

                  if (availableShippingRates.length) {
                    newCheckedShippings[cart.shop.myshopifyDomain] =
                      availableShippingRates[0];

                    newShippingCostState.push(
                      parseFloat(availableShippingRates[0].price.amount)
                    );
                  }
                }

                return data;
              }

              return {};
            })
            .catch((err) =>
              console.error(`Error getting shipping rates: ${err}`)
            )
        );
      }

      Promise.all(calculated_draft_orders_promises).then((res) => {
        console.log("calculated_draft_orders_promises res", res);

        const subtotal = res
          .map((draft_order) => draft_order?.draftOrderCalculate.subtotalPrice)
          .reduce((acc, curr) => parseFloat(acc) + parseFloat(curr));

        const taxes = res
          .map((draft_order) => draft_order?.draftOrderCalculate.totalTax)
          .reduce((acc, curr) => parseFloat(acc) + parseFloat(curr));

        setSubtotalState(subtotal);
        setTaxesState(taxes);
        setShippingsState(newShippingsState);
        setCheckedShippings(newCheckedShippings);
      });
    }
  }, [groupedCartartData, addressState]);

  useEffect(() => {
    setShippingCostState(
      Object.keys(checkedShippings)
        .map((domain) => parseFloat(checkedShippings[domain].price.amount))
        .reduce((accumulated, current) => accumulated + current, 0)
    );
  }, [checkedShippings]);

  useEffect(() => {
    getFavicons(domains).then((res) => setFaviconState(res));
  }, [domains]);

  /**
   * ================================
   * Custom methods
   * ================================
   */
  const onPaymentConfirm = async () => {
    if (groupedCartartData?.getGroupedCart) {
      const createOrderLoadingMessage = notify({
        message: `Creating orders`,
        type: "loading",
      });

      const data: CompleteShopCheckoutInput[] = [];

      // Delete default and _id properties from address object for matching MailingAddressInput type
      const cleanAddress = JSON.parse(JSON.stringify(addressState));
      delete cleanAddress[`default`];
      delete cleanAddress[`_id`];

      for (const cart of groupedCartartData.getGroupedCart) {
        if (cart.items) {
          const items = cart.items.map((item) => {
            return {
              variantId: item.id,
              quantity: item.quantity || 1,
            };
          });

          const shop_data: CompleteShopCheckoutInput = {
            shop: cart.shop.myshopifyDomain,
            items,
            shippingAddress: cleanAddress,
            shippingLine: checkedShippings[cart.shop.myshopifyDomain].handle,
          };

          data.push(shop_data);
        }
      }

      completeCheckout({
        variables: {
          data,
          paymentMethod: selectedCardState?.id,
        },
      })
        .then(({ data: completeCheckoutData }) => {
          notify({
            message: `Orders created`,
            type: "success",
          });

          history.push({
            pathname: "/order-confirmed",
            state: {
              orders: completeCheckoutData.completeCheckout,
            },
          });
        })
        .catch((err) => {
          notify({
            message: `Error creating orders`,
            type: "error",
          });
          console.error("Error creating orders: ", err);
        })
        .finally(() =>
          notify({ type: "dismiss", toastId: createOrderLoadingMessage })
        );
    } else {
      console.error("No cart");
    }
  };

  return (
    <CheckoutStyle>
      <Heading as="h1" size="lg" lineHeight="taller" marginBottom={4}>
        Checkout
      </Heading>
      <Grid gap={20} marginBottom={20} templateColumns="5fr 2fr">
        <VStack align="strech" spacing={12}>
          <VStack align="strech" spacing={4}>
            <HStack
              justifyContent="space-between"
              spacing={2}
              paddingBottom={2}
              borderBottom="1px"
              borderColor="gray.200"
            >
              <Heading as="h2" size="md">
                Address
              </Heading>
              <ChakraButton
                colorScheme="gray"
                size="xs"
                leftIcon={<IoPencil />}
                onClick={onShippingModalOpen}
              >
                Edit
              </ChakraButton>
            </HStack>
            <VStack align="stretch" spacing={1}>
              {userLoading ? (
                <HStack spacing={4}>
                  <Spinner size="sm" emptyColor="gray.200" />
                  <Text color="gray.500">Loading shipping addresses...</Text>
                </HStack>
              ) : !addressState ? (
                <HStack spacing={4}>
                  <Center
                    backgroundColor="red.200"
                    p={2}
                    borderRadius="full"
                    width="30px"
                    height="30px"
                  >
                    <Icon color="red.500" as={IoAlert} />
                  </Center>
                  <Text color="gray.500">No shipping address available</Text>
                </HStack>
              ) : (
                <>
                  {addressState.default && (
                    <Badge
                      width="fit-content"
                      colorScheme="blue"
                      borderRadius="md"
                    >
                      Default
                    </Badge>
                  )}
                  <VStack align="stretch" spacing={0}>
                    <Text fontSize="sm" fontWeight={500}>
                      {`${addressState.firstName} ${addressState.lastName}`}
                    </Text>
                    <Text fontSize="sm">{`${addressState.address1}, ${addressState.address2}`}</Text>
                    <Text fontSize="sm">{`${addressState.zip} ${addressState.city}, ${addressState.province}, ${addressState.country}`}</Text>
                  </VStack>
                </>
              )}
            </VStack>
          </VStack>
          <VStack align="stretch" spacing={4}>
            <HStack
              justifyContent="space-between"
              spacing={2}
              paddingBottom={2}
              borderBottom="1px"
              borderColor="gray.200"
            >
              <Heading as="h2" size="md">
                Payment
              </Heading>
              <ChakraButton
                colorScheme="gray"
                size="xs"
                leftIcon={<IoPencil />}
                onClick={onPaymentModalOpen}
              >
                Edit
              </ChakraButton>
            </HStack>
            <VStack align="stretch" spacing={1}>
              {paymentMethodsLoading ? (
                <HStack spacing={4}>
                  <Spinner size="sm" emptyColor="gray.200" />
                  <Text color="gray.500">Loading payment methods...</Text>
                </HStack>
              ) : !selectedCardState ? (
                <HStack spacing={4}>
                  <Center
                    backgroundColor="red.200"
                    p={2}
                    borderRadius="full"
                    width="30px"
                    height="30px"
                  >
                    <Icon color="red.500" as={IoAlert} />
                  </Center>
                  <Text color="gray.500">No payment methods available</Text>
                </HStack>
              ) : (
                <HStack spacing={4}>
                  <Image
                    width="48px"
                    src={images.paymentMethods[selectedCardState.card!.brand!]}
                  />
                  <VStack spacing={0} align="flex-start">
                    <HStack spacing={2}>
                      <Text fontSize="sm" fontWeight={500}>
                        <span style={{ textTransform: "capitalize" }}>
                          {selectedCardState.card?.brand}
                        </span>{" "}
                        card ending in {selectedCardState.card?.last4}
                      </Text>
                      {selectedCardState.default && (
                        <Badge
                          width="fit-content"
                          colorScheme="blue"
                          borderRadius="md"
                        >
                          Default
                        </Badge>
                      )}
                    </HStack>
                    <Text fontSize="sm" color="gray.500">
                      Expires {selectedCardState.card?.exp_month}/
                      {selectedCardState.card?.exp_year}
                    </Text>
                  </VStack>
                </HStack>
              )}
            </VStack>
          </VStack>
          <VStack align="stretch" spacing={4}>
            <HStack
              justifyContent="space-between"
              spacing={2}
              paddingBottom={2}
              borderBottom="1px"
              borderColor="gray.200"
            >
              <Heading as="h2" size="md">
                Products and shipping
              </Heading>
            </HStack>
            <VStack
              align="stretch"
              spacing={4}
              divider={<Divider borderColor="gray.200" />}
            >
              {groupedCartartLoading ? (
                <VStack align="center" marginY={6} spacing={6}>
                  <Spinner size="sm" emptyColor="gray.200" />
                  <Text color="gray.500">Loading items...</Text>
                </VStack>
              ) : groupedCartartData?.getGroupedCart &&
                groupedCartartData?.getGroupedCart.length > 0 ? (
                groupedCartartData.getGroupedCart.map((cart) => (
                  <VStack
                    align="stretch"
                    spacing={4}
                    key={cart.shop.myshopifyDomain}
                  >
                    <HStack align="center" spacing={3}>
                      <AspectRatio
                        alignItems="center"
                        width="24px"
                        ratio={1 / 1}
                      >
                        {faviconState[cart.shop.myshopifyDomain] ? (
                          <Image
                            src={faviconState[cart.shop.myshopifyDomain]}
                            alt=""
                            objectFit="cover"
                          />
                        ) : (
                          <Icon
                            color="gray.400"
                            width="100%"
                            height="100%"
                            as={IoBan}
                          />
                        )}
                      </AspectRatio>
                      <Heading as="h3" size="sm" lineHeight="tall">
                        <Link
                          href={`https://${cart.shop.myshopifyDomain}`}
                          isExternal
                        >
                          {cart.shop.name} <Icon as={HiExternalLink} mx="2px" />
                        </Link>
                      </Heading>
                    </HStack>
                    <SimpleGrid columns={2} spacing={10}>
                      <VStack spacing={2} align="strech">
                        <Heading
                          as="h3"
                          size="xs"
                          lineHeight="tall"
                          textTransform="uppercase"
                          fontWeight={600}
                          colorScheme="gray"
                        >
                          Shipping
                        </Heading>
                        <VStack align="strech" spacing={4}>
                          {Object.keys(shippingsState).length ? (
                            shippingsState[cart.shop.myshopifyDomain] &&
                            shippingsState[cart.shop.myshopifyDomain].length ? (
                              shippingsState[cart.shop.myshopifyDomain].map(
                                (shipping, shippingIndex) => (
                                  <Checkbox
                                    key={`shipping-${shippingIndex}`}
                                    border="1px"
                                    borderColor="gray.300"
                                    borderRadius={6}
                                    p={3}
                                    spacing={3}
                                    isChecked={
                                      checkedShippings[
                                        cart.shop.myshopifyDomain
                                      ]?.handle === shipping.handle
                                    }
                                    onChange={() =>
                                      setCheckedShippings({
                                        ...checkedShippings,
                                        [cart.shop.myshopifyDomain]: shipping,
                                      })
                                    }
                                  >
                                    <Text fontSize="sm" fontWeight={500}>
                                      {shipping.title} - {shipping.price.amount}{" "}
                                      {shipping.price.currencyCode}
                                    </Text>
                                  </Checkbox>
                                )
                              )
                            ) : (
                              <HStack spacing={2}>
                                <Icon
                                  as={IoAlertCircleOutline}
                                  color="red.500"
                                />
                                <Text color="red.500">No shipping options</Text>
                              </HStack>
                            )
                          ) : (
                            <HStack spacing={4}>
                              <Spinner size="sm" emptyColor="gray.200" />
                              <Text color="gray.500">
                                Loading shipping options...
                              </Text>
                            </HStack>
                          )}
                        </VStack>
                      </VStack>
                      <VStack spacing={2} align="strech">
                        <Heading
                          as="h3"
                          size="xs"
                          lineHeight="tall"
                          textTransform="uppercase"
                          fontWeight={600}
                        >
                          Products
                        </Heading>
                        <VStack align="stretch" spacing={4}>
                          {cart.items?.map((item, itemIndex) => (
                            <HStack
                              spacing={4}
                              alignItems="center"
                              key={`${itemIndex}-${item.id}`}
                            >
                              <AspectRatio
                                alignItems="center"
                                minWidth="56px"
                                ratio={1.25 / 2}
                                borderRadius={8}
                                overflow="hidden"
                              >
                                {item.product.images.edges.length ? (
                                  <Image
                                    src={
                                      item.product.images.edges[0].node
                                        .originalSrc
                                    }
                                    alt=""
                                    objectFit="cover"
                                  />
                                ) : (
                                  <Icon
                                    width="100%"
                                    height="100%"
                                    as={IoIosPricetag}
                                    color="gray.500"
                                  />
                                )}
                              </AspectRatio>
                              <VStack
                                spacing={2}
                                justify="space-between"
                                alignItems="start"
                              >
                                <VStack spacing={0} alignItems="start">
                                  <Text fontSize="md" fontWeight={500}>
                                    {item.displayName}
                                  </Text>
                                  <Text fontSize="sm" color="gray.600">
                                    Quantity: <b>{item.quantity}</b>
                                  </Text>
                                </VStack>
                                <Text fontSize="sm" color="gray.600">
                                  <CurrencyNumber
                                    locales="es-ES"
                                    currency="EUR"
                                    amount={
                                      item.presentmentPrices.edges[0].node.price
                                        .amount
                                    }
                                  />
                                </Text>
                              </VStack>
                            </HStack>
                          ))}
                        </VStack>
                      </VStack>
                    </SimpleGrid>
                  </VStack>
                ))
              ) : (
                <VStack align="center" marginY={6} spacing={6}>
                  <Icon
                    width="42px"
                    height="42px"
                    color="blue.500"
                    as={IoIosCart}
                  />
                  <VStack>
                    <Heading as="h3" size="md" lineHeight="normal">
                      There are no items in your cart
                    </Heading>
                    <Text fontWeight={400} color="gray.400">
                      Let's go shopping!
                    </Text>
                  </VStack>
                </VStack>
              )}
            </VStack>
          </VStack>
        </VStack>
        <VStack align="stretch" spacing={4}>
          <Heading
            as="h2"
            size="md"
            paddingBottom={2}
            borderBottom="1px"
            borderColor="gray.200"
          >
            Confirm order
          </Heading>
          <VStack align="stretch" spacing={2}>
            <HStack justifyContent="space-between" spacing={2}>
              <Text fontSize="sm" color="gray.500" fontWeight="500">
                Products total
              </Text>
              <Text fontSize="sm" fontWeight="500">
                {groupedCartartLoading ? (
                  <Spinner size="sm" emptyColor="gray.200" />
                ) : (
                  <CurrencyNumber
                    locales="es-ES"
                    currency="EUR"
                    amount={subtotalState}
                  />
                )}
              </Text>
            </HStack>
            <Stack
              width="100%"
              direction="row"
              justifyContent="space-between"
              spacing={2}
            >
              <Text fontSize="sm" color="gray.500" fontWeight="500">
                Shipping cost
              </Text>
              <Text fontSize="sm" fontWeight="500">
                {groupedCartartLoading ? (
                  <Spinner size="sm" emptyColor="gray.200" />
                ) : (
                  <CurrencyNumber
                    locales="es-ES"
                    currency="EUR"
                    amount={shippingCostState}
                  />
                )}
              </Text>
            </Stack>
          </VStack>
          <Divider color="gray.200" />
          <VStack align="stretch" spacing={2}>
            <HStack justifyContent="space-between" spacing={2}>
              <Text fontWeight="500">Total</Text>
              <Text fontWeight="500">
                {groupedCartartLoading ? (
                  <Spinner size="sm" emptyColor="gray.200" />
                ) : (
                  <CurrencyNumber
                    locales="es-ES"
                    currency="EUR"
                    amount={subtotalState + shippingCostState}
                  />
                )}
              </Text>
            </HStack>
            <HStack justifyContent="flex-end" spacing={2}>
              <Text fontSize="smaller" color="gray.500" fontWeight="400">
                Includes{" "}
                <CurrencyNumber
                  locales="es-ES"
                  currency="EUR"
                  amount={taxesState}
                />{" "}
                in taxes
              </Text>
            </HStack>
          </VStack>
          {/* <Button
            onClick={() => onPaymentConfirm()}
            text="Confirm payment →"
            size="medium"
            fullWidth
          /> */}
          <ChakraButton
            size="md"
            colorScheme="blue"
            isFullWidth
            onClick={() => onPaymentConfirm()}
            isLoading={completeCheckoutLoading}
            isDisabled={
              !groupedCartartData?.getGroupedCart.length ||
              subtotalState + shippingCostState === 0
            }
          >
            Confirm payment
          </ChakraButton>
        </VStack>
      </Grid>
      <Modal
        isOpen={isShippingModalOpen}
        onClose={onShippingModalClose}
        size="lg"
        isCentered
      >
        <ModalOverlay />
        <ModalContent pb={2}>
          <ModalHeader>Choose address</ModalHeader>
          <ModalCloseButton top={4} right={6} />
          <ModalBody>
            <VStack
              align="stretch"
              spacing={3}
              divider={<StackDivider borderBottom="1px" color="gray.200" />}
            >
              {userData?.getUser.shippingAddresses?.length ? (
                userData?.getUser.shippingAddresses.map(
                  (address, addressIndex: number) => (
                    <HStack
                      justifyContent="space-between"
                      key={`address-${addressIndex}`}
                    >
                      <VStack spacing={2} align="flex-start">
                        {address.default && (
                          <Badge
                            width="fit-content"
                            colorScheme="blue"
                            borderRadius="md"
                          >
                            Default
                          </Badge>
                        )}
                        <VStack align="stretch" spacing={0}>
                          <Text fontSize="sm" fontWeight={500}>
                            {`${address.firstName} ${address.lastName}`}
                          </Text>
                          <Text fontSize="sm">{`${address.address1}, ${address.address2}`}</Text>
                          <Text fontSize="sm">{`${address.zip} ${address.city}, ${address.province}, ${address.country}`}</Text>
                        </VStack>
                      </VStack>
                      <HStack>
                        <ChakraButton
                          size="sm"
                          colorScheme="blue"
                          disabled={address === addressState}
                          onClick={() => {
                            setAddressState(address);
                            onShippingModalClose();
                          }}
                        >
                          Select
                        </ChakraButton>
                      </HStack>
                    </HStack>
                  )
                )
              ) : (
                <HStack spacing={4}>
                  <AspectRatio
                    width="30px"
                    ratio={1 / 1}
                    backgroundColor="red.200"
                    p={2}
                    borderRadius="full"
                    justifyContent="center"
                  >
                    <Icon color="red.500" width="100%" as={IoAlert} />
                  </AspectRatio>
                  <Text color="gray.500">No payment methods added</Text>
                </HStack>
              )}
            </VStack>
          </ModalBody>
        </ModalContent>
      </Modal>
      <Modal
        isOpen={isPaymentModalOpen}
        onClose={onPaymentModalClose}
        size="lg"
        isCentered
      >
        <ModalOverlay />
        <ModalContent pb={2}>
          <ModalHeader>Choose payment method</ModalHeader>
          <ModalCloseButton top={4} right={6} />
          <ModalBody>
            <VStack
              align="stretch"
              spacing={3}
              divider={<StackDivider borderBottom="1px" color="gray.200" />}
            >
              {paymentMethodsData?.getUserPaymentMethods.length ? (
                paymentMethodsData.getUserPaymentMethods.map(
                  (paymentMethod, paymentMethodIndex: number) => (
                    <HStack
                      justifyContent="space-between"
                      key={`payment-${paymentMethodIndex}`}
                    >
                      <HStack spacing={3}>
                        <Image
                          width="48px"
                          src={
                            images.paymentMethods[paymentMethod.card?.brand!]
                          }
                        />
                        <VStack spacing={0} align="flex-start">
                          <HStack spacing={2}>
                            <Text fontSize="sm" fontWeight={500}>
                              <span style={{ textTransform: "capitalize" }}>
                                {paymentMethod.card?.brand}
                              </span>{" "}
                              card ending in {paymentMethod.card?.last4}
                            </Text>
                            {paymentMethod.default && (
                              <Badge
                                width="fit-content"
                                colorScheme="blue"
                                borderRadius="md"
                              >
                                Default
                              </Badge>
                            )}
                          </HStack>
                          <Text fontSize="sm" color="gray.500">
                            Expires {paymentMethod.card?.exp_month}/
                            {paymentMethod.card?.exp_year}
                          </Text>
                        </VStack>
                      </HStack>
                      <HStack>
                        <ChakraButton
                          size="sm"
                          colorScheme="blue"
                          disabled={paymentMethod.id === selectedCardState?.id}
                          onClick={() => {
                            setSelectedCardState(paymentMethod);
                            onPaymentModalClose();
                          }}
                        >
                          Select
                        </ChakraButton>
                      </HStack>
                    </HStack>
                  )
                )
              ) : (
                <HStack spacing={4}>
                  <AspectRatio
                    width="30px"
                    ratio={1 / 1}
                    backgroundColor="red.200"
                    p={2}
                    borderRadius="full"
                    justifyContent="center"
                  >
                    <Icon color="red.500" width="100%" as={IoAlert} />
                  </AspectRatio>
                  <Text color="gray.500">No payment methods added</Text>
                </HStack>
              )}
            </VStack>
          </ModalBody>
        </ModalContent>
      </Modal>
    </CheckoutStyle>
  );
};

export default Checkout;
