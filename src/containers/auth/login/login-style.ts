import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";
import { BodySmallMixin } from "../../../components/styled/text/body-text";

export const LoginStyle = styled.div`
  .login-content {
    display: flex;
    flex-direction: column;

    .login-content-heading {
      margin-bottom: 20px;

      .login-content-heading-title {
        margin-bottom: 12px;
      }

      .login-content-heading-subtitle {
        color: ${colors.bn.gray_700};
      }
    }

    .login-content-form {
      &__submit {
        margin-top: 20px;
      }
    }

    .login-content-action {
      align-self: flex-end;
      ${BodySmallMixin};
      margin-top: 32px;
    }
  }
`;
