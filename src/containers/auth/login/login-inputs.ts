import { isValidEmail } from "../../../utils/validation";

export const LOGIN_EMAIL_INPUT_NAME = "email";
export const LOGIN_PASSWORD_INPUT_NAME = "password";

export const getLoginFormInputMetadata = (
  inputName: string,
  extraValidation: any = {}
): { [key: string]: any } => {
  const inputsArray: any = {
    [LOGIN_EMAIL_INPUT_NAME]: {
      required: { value: true, message: "required" },
      validate: {
        isValidEmail: (value: any) => isValidEmail(value),
      },
    },
    [LOGIN_PASSWORD_INPUT_NAME]: {
      required: { value: true, message: "required" },
      // minLength: { value: 8, message: "too-short-password" },
    },
  };

  return inputsArray[inputName];
};

export const getLoginFormErrors = (inputErrors: any, inputName: string) => {
  if (!inputErrors[inputName]) return;

  if (inputErrors[inputName]?.type === "isValidEmail") return "invalid-email";
  if (inputErrors[inputName]?.type === "isValidPassword")
    return "invalid-password";
  if (inputErrors[inputName]?.type === "passwordMissmatch") return "no-match";

  return inputErrors[inputName]?.message;
};
