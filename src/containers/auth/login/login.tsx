import { useMutation } from "@apollo/react-hooks";
import {
  Alert,
  AlertIcon,
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  IconButton,
  Input,
  InputGroup,
  InputRightElement,
  Link,
  Text,
  VStack,
} from "@chakra-ui/react";
import md5 from "md5";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { IoEyeOffSharp, IoEyeSharp } from "react-icons/io5";
import { Link as RouterLink } from "react-router-dom";
import { colors } from "../../../assets/colors/colors";
import { LOGIN__MUTATION } from "../../../gql/mutations";
import {
  getLoginFormErrors,
  getLoginFormInputMetadata,
  LOGIN_EMAIL_INPUT_NAME,
  LOGIN_PASSWORD_INPUT_NAME,
} from "./login-inputs";

interface FormData {
  email: string;
  password: string;
}

const Login = () => {
  const [loginUser, { loading: loginUserLoading, error: loginUserError }] =
    useMutation(LOGIN__MUTATION);

  const [showPassword, setShowPassword] = useState(false);

  const {
    register,
    handleSubmit,
    errors,
    formState: { isDirty },
  } = useForm<FormData>();

  const onSubmit = async (formData: FormData) => {
    console.log("errors", errors);

    loginUser({
      variables: {
        email: formData[LOGIN_EMAIL_INPUT_NAME],
        password: md5(formData[LOGIN_PASSWORD_INPUT_NAME]),
      },
    })
      .then((res) => {
        console.log("Login | ✅", res);
        localStorage.setItem("centollUser", JSON.stringify(res.data.login));
        localStorage.setItem("_centoll_userId", res.data.login._id);
        localStorage.setItem("_centoll_jwt", res.data.login.token);
        localStorage.setItem("_centoll_email", res.data.login.email);
        window.location.reload();
      })
      .catch((err) => console.error("Login error: ", err));
  };

  return (
    <>
      <VStack align="start">
        <Text as="h3" fontSize="xl" fontWeight={700}>
          Sign in to your account
        </Text>
        <Text fontSize="sm" fontWeight={500} color="gray.700">
          New to Centoll?
          <Link to="/signup" as={RouterLink} color={colors.brand.primary}>
            {"  "}Get started
          </Link>
        </Text>
      </VStack>
      <VStack align="stretch" width="100%" spacing={6}>
        <form>
          <FormControl isInvalid={Boolean(errors.email)} mt={4}>
            <FormLabel
              htmlFor={LOGIN_EMAIL_INPUT_NAME}
              fontSize="sm"
              mb={1}
              color="gray.700"
            >
              Email
            </FormLabel>
            <Input
              name={LOGIN_EMAIL_INPUT_NAME}
              placeholder="Email"
              type={"email"}
              ref={register(getLoginFormInputMetadata(LOGIN_EMAIL_INPUT_NAME))}
            />
            <FormErrorMessage>
              {getLoginFormErrors(errors, LOGIN_EMAIL_INPUT_NAME)}
            </FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={Boolean(errors.password)} mt={4}>
            <FormLabel
              htmlFor={LOGIN_PASSWORD_INPUT_NAME}
              fontSize="sm"
              mb={1}
              color="gray.700"
            >
              Password
            </FormLabel>
            <InputGroup>
              <Input
                name={LOGIN_PASSWORD_INPUT_NAME}
                placeholder="Password"
                type={showPassword ? "text" : "password"}
                ref={register(
                  getLoginFormInputMetadata(LOGIN_PASSWORD_INPUT_NAME)
                )}
              />
              <InputRightElement p={2}>
                <IconButton
                  aria-label="Show password"
                  size="sm"
                  color="gray.500"
                  icon={showPassword ? <IoEyeOffSharp /> : <IoEyeSharp />}
                  onClick={() => setShowPassword(!showPassword)}
                />
              </InputRightElement>
            </InputGroup>
            <FormErrorMessage>
              {getLoginFormErrors(errors, LOGIN_PASSWORD_INPUT_NAME)}
            </FormErrorMessage>
          </FormControl>
          <Button
            colorScheme="blue"
            disabled={!isDirty}
            isLoading={loginUserLoading}
            type="submit"
            mt={6}
            isFullWidth
            onClick={handleSubmit(onSubmit)}
          >
            Sign in
          </Button>
        </form>
        {loginUserError && (
          <Alert
            status="error"
            variant="left-accent"
            borderRadius={4}
            py={2}
            mt={10}
          >
            <AlertIcon w={4} />
            {loginUserError.message}
          </Alert>
        )}
      </VStack>
      {/* <LoginStyle>
        <div className="login-content">
          <form
            className="login-content-form"
            onSubmit={(e) => {
              e.preventDefault();
              console.log("submit");
            }}
          >
            <Input
              className="login-content-form__input"
              label="Email"
              size="large"
              name={LOGIN_EMAIL_INPUT_NAME}
              inputRef={register(
                getLoginFormInputMetadata(LOGIN_EMAIL_INPUT_NAME)
              )}
              errorMessage={getLoginFormErrors(errors, LOGIN_EMAIL_INPUT_NAME)}
            />

            <Input
              className="login-content-form__input"
              label="Password"
              type="password"
              size="large"
              name={LOGIN_PASSWORD_INPUT_NAME}
              inputRef={register(
                getLoginFormInputMetadata(LOGIN_PASSWORD_INPUT_NAME)
              )}
              errorMessage={getLoginFormErrors(
                errors,
                LOGIN_PASSWORD_INPUT_NAME
              )}
            />
            <Button
              className="login-content-form__submit"
              onClick={handleSubmit(onSubmit)}
              type="primary"
              text="Sign in"
              disabled={!isDirty}
              isSubmit
              fullWidth
            />
          </form>
          <div className="login-content-action">
            <Link to="/forgot-password">Forgot Your Password?</Link>
          </div>
        </div>
      </LoginStyle> */}
    </>
  );
};

export default Login;
