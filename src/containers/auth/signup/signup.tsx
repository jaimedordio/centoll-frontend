import { useMutation } from "@apollo/react-hooks";
import {
  Alert,
  AlertIcon,
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  IconButton,
  Input,
  InputGroup,
  InputRightElement,
  Link,
  SimpleGrid,
  Text,
  VStack,
} from "@chakra-ui/react";
import md5 from "md5";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { IoEyeOffSharp, IoEyeSharp } from "react-icons/io5";
import { Link as RouterLink } from "react-router-dom";
import { colors } from "../../../assets/colors/colors";
import { SIGNUP__MUTATION } from "../../../gql/mutations";
import { notify } from "../../../utils/toast";
import {
  getSignupFormErrors,
  getSignupFormInputMetadata,
  SIGNUP_EMAIL_INPUT_NAME,
  SIGNUP_FIRST_NAME_INPUT_NAME,
  SIGNUP_LAST_NAME_INPUT_NAME,
  SIGNUP_PASSWORD_INPUT_NAME,
} from "./signup.inputs";

interface SignupForm {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

const Signup = () => {
  const [signupUser, { loading: signupUserLoading, error: signupUserError }] =
    useMutation(SIGNUP__MUTATION);

  const [showPassword, setShowPassword] = useState(false);

  const {
    register,
    handleSubmit,
    errors,
    formState: { isDirty },
  } = useForm<SignupForm>();

  const onSubmit = async (formData: SignupForm) => {
    console.log("errors", errors);

    signupUser({
      variables: {
        firstName: formData[SIGNUP_FIRST_NAME_INPUT_NAME],
        lastName: formData[SIGNUP_LAST_NAME_INPUT_NAME],
        email: formData[SIGNUP_EMAIL_INPUT_NAME],
        password: md5(formData[SIGNUP_PASSWORD_INPUT_NAME]),
      },
    })
      .then((res) => {
        notify({ message: "Successfully registeered", type: "success" });
        console.log("Signup | ✅", res);
        localStorage.setItem("centollUser", JSON.stringify(res.data.login));
        localStorage.setItem("_centoll_userId", res.data.login._id);
        localStorage.setItem("_centoll_jwt", res.data.login.token);
        localStorage.setItem("_centoll_email", res.data.login.email);
        window.location.reload();
      })
      .catch((err) => console.error("Signup error: ", err));
  };

  return (
    <>
      <VStack align="start">
        <Text as="h3" fontSize="xl" fontWeight={700}>
          Create a new account
        </Text>
        <Text fontSize="sm" fontWeight={500} color="gray.700">
          Already have an account?
          <Link to="/login" as={RouterLink} color={colors.brand.primary}>
            {"  "}Login
          </Link>
        </Text>
      </VStack>
      <VStack align="stretch" width="100%" spacing={6}>
        <form>
          <SimpleGrid columns={2} spacingX={4}>
            <FormControl
              isInvalid={Boolean(errors[SIGNUP_FIRST_NAME_INPUT_NAME])}
            >
              <FormLabel
                htmlFor={SIGNUP_FIRST_NAME_INPUT_NAME}
                fontSize="sm"
                mb={1}
                color="gray.700"
              >
                First Name
              </FormLabel>

              <Input
                size="md"
                fontSize="sm"
                borderRadius="md"
                name={SIGNUP_FIRST_NAME_INPUT_NAME}
                placeholder="First name"
                ref={register(
                  getSignupFormInputMetadata(SIGNUP_FIRST_NAME_INPUT_NAME)
                )}
              />
              <FormErrorMessage>
                {getSignupFormErrors(errors, SIGNUP_FIRST_NAME_INPUT_NAME)}
              </FormErrorMessage>
            </FormControl>
            <FormControl
              isInvalid={Boolean(errors[SIGNUP_LAST_NAME_INPUT_NAME])}
            >
              <FormLabel
                htmlFor={SIGNUP_LAST_NAME_INPUT_NAME}
                fontSize="sm"
                mb={1}
                color="gray.700"
              >
                Last Name
              </FormLabel>

              <Input
                size="md"
                fontSize="sm"
                borderRadius="md"
                name={SIGNUP_LAST_NAME_INPUT_NAME}
                placeholder="Last name"
                ref={register(
                  getSignupFormInputMetadata(SIGNUP_LAST_NAME_INPUT_NAME)
                )}
              />
              <FormErrorMessage>
                {getSignupFormErrors(errors, SIGNUP_LAST_NAME_INPUT_NAME)}
              </FormErrorMessage>
            </FormControl>
          </SimpleGrid>

          <FormControl isInvalid={Boolean(errors.email)} mt={4}>
            <FormLabel
              htmlFor={SIGNUP_EMAIL_INPUT_NAME}
              fontSize="sm"
              mb={1}
              color="gray.700"
            >
              Email
            </FormLabel>
            <Input
              name={SIGNUP_EMAIL_INPUT_NAME}
              placeholder="Email"
              type={"email"}
              ref={register(
                getSignupFormInputMetadata(SIGNUP_EMAIL_INPUT_NAME)
              )}
            />
            <FormErrorMessage>
              {getSignupFormErrors(errors, SIGNUP_EMAIL_INPUT_NAME)}
            </FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={Boolean(errors.password)} mt={4}>
            <FormLabel
              htmlFor={SIGNUP_PASSWORD_INPUT_NAME}
              fontSize="sm"
              mb={1}
              color="gray.700"
            >
              Password
            </FormLabel>
            <InputGroup>
              <Input
                name={SIGNUP_PASSWORD_INPUT_NAME}
                placeholder="Password"
                type={showPassword ? "text" : "password"}
                ref={register(
                  getSignupFormInputMetadata(SIGNUP_PASSWORD_INPUT_NAME)
                )}
              />
              <InputRightElement p={2}>
                <IconButton
                  aria-label="Show password"
                  size="sm"
                  color="gray.500"
                  icon={showPassword ? <IoEyeOffSharp /> : <IoEyeSharp />}
                  onClick={() => setShowPassword(!showPassword)}
                />
              </InputRightElement>
            </InputGroup>
            <FormErrorMessage>
              {getSignupFormErrors(errors, SIGNUP_PASSWORD_INPUT_NAME)}
            </FormErrorMessage>
          </FormControl>
          <Button
            colorScheme="blue"
            disabled={!isDirty}
            isLoading={signupUserLoading}
            type="submit"
            mt={6}
            isFullWidth
            onClick={handleSubmit(onSubmit)}
          >
            Sign up
          </Button>
        </form>
        {signupUserError && (
          <Alert
            status="error"
            variant="left-accent"
            borderRadius={4}
            py={2}
            mt={10}
          >
            <AlertIcon w={4} />
            {signupUserError.message}
          </Alert>
        )}
      </VStack>
    </>
  );
};

export default Signup;
