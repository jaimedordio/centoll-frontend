import { isValidEmail } from "../../../utils/validation";

export const SIGNUP_FIRST_NAME_INPUT_NAME = "firstName";
export const SIGNUP_LAST_NAME_INPUT_NAME = "lastName";
export const SIGNUP_EMAIL_INPUT_NAME = "email";
export const SIGNUP_PASSWORD_INPUT_NAME = "password";

export const getSignupFormInputMetadata = (
  inputName: string,
  extraValidation: any = {}
): { [key: string]: any } => {
  const inputsArray: any = {
    [SIGNUP_FIRST_NAME_INPUT_NAME]: {
      required: { value: true, message: "required" },
    },
    [SIGNUP_LAST_NAME_INPUT_NAME]: {
      required: { value: true, message: "required" },
    },
    [SIGNUP_EMAIL_INPUT_NAME]: {
      required: { value: true, message: "required" },
      validate: {
        isValidEmail: (value: any) => isValidEmail(value),
      },
    },
    [SIGNUP_PASSWORD_INPUT_NAME]: {
      required: { value: true, message: "required" },
      // minLength: { value: 8, message: "too-short-password" },
    },
  };

  return inputsArray[inputName];
};

export const getSignupFormErrors = (inputErrors: any, inputName: string) => {
  if (!inputErrors[inputName]) return;

  if (inputErrors[inputName]?.type === "isValidEmail") return "invalid-email";
  if (inputErrors[inputName]?.type === "isValidPassword")
    return "invalid-password";
  if (inputErrors[inputName]?.type === "passwordMissmatch") return "no-match";

  return inputErrors[inputName]?.message;
};
