import { ChakraProvider } from "@chakra-ui/react";
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import { chakraCustomTheme } from "./utils/chakra-theme";

Sentry.init({
  dsn:
    "https://52e454444c034a8c85a2f24986bfd9f1@o534327.ingest.sentry.io/5657085",
  integrations: [new Integrations.BrowserTracing()],
});

ReactDOM.render(
  <React.StrictMode>
    <ChakraProvider theme={chakraCustomTheme}>
      <App />
    </ChakraProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
