import { useMutation } from "@apollo/react-hooks";
import md5 from "md5";
import {
  LOGIN__MUTATION,
  LOGOUT__MUTATION,
  VALIDATE_USER_SESSION__MUTATION,
} from "../gql/mutations";
import { apolloClient } from "../utils/apollo-client";

const UserServices = () => {
  const [
    loginUser,
    { loading: loginUserLoading, error: loginUserError, data: loginUserData },
  ] = useMutation(LOGIN__MUTATION, {
    client: apolloClient,
  });

  const [validateUser] = useMutation(VALIDATE_USER_SESSION__MUTATION, {
    client: apolloClient,
  });

  const [
    logoutUser,
    {
      loading: logoutUserLoading,
      error: logoutUserError,
      data: logoutUserData,
    },
  ] = useMutation(LOGOUT__MUTATION, {
    client: apolloClient,
  });

  const login = async (emailArg: string, passwordArg: string) => {
    await loginUser({
      variables: {
        email: emailArg,
        password: md5(passwordArg),
      },
    })
      .then((res) => {
        console.log("Login | ✅", res);
        localStorage.setItem("centollUser", JSON.stringify(res.data.login));
        localStorage.setItem("_centoll_userId", res.data.login._id);
        localStorage.setItem("_centoll_jwt", res.data.login.token);
        localStorage.setItem("_centoll_email", res.data.login.email);
        window.location.reload();
      })
      .catch((e) => {
        console.error("Login | ❌", e);
      });

    return {
      loading: loginUserLoading,
      error: loginUserError,
      data: loginUserData,
    };
  };

  const currentSession = async () => {
    return await validateUser();
  };

  const logout = async () => {
    logoutUser()
      .then((res) => {
        console.log("Logout | ✅", res);
        localStorage.removeItem("centollUser");
        localStorage.removeItem("_centoll_userId");
        localStorage.removeItem("_centoll_jwt");
        localStorage.removeItem("_centoll_email");

        window.location.reload();
      })
      .catch((e) => {
        console.error("Logout | ❌", e);
      });

    return {
      loading: logoutUserLoading,
      error: logoutUserError,
      data: logoutUserData,
    };
  };

  return {
    login,
    currentSession,
    logout,
  };
};

export default UserServices;
