import { gql } from "apollo-boost";

export const USER__QUERY = gql`
  query {
    getUser {
      _id
      firstName
      lastName
      email
      userSince
      shippingAddresses {
        _id
        address1
        address2
        city
        country
        provinceCode
        zip
        firstName
        lastName
        default
      }
    }
  }
`;

export const PAYMENT_METHODS__QUERY = gql`
  query {
    getUserPaymentMethods {
      billing_details {
        address {
          city
          country
          line1
          line2
          postal_code
          state
        }
        email
        name
        phone
      }
      card {
        address_city
        address_country
        address_line1
        address_line1_check
        address_line2
        address_state
        address_zip
        address_zip_check
        brand
        country
        customer
        cvc_check
        dynamic_last4
        exp_month
        exp_year
        fingerprint
        funding
        id
        last4
        name
        tokenization_method
      }
      created
      customer
      default
      id
      livemode
    }
  }
`;

export const SHIPPING_ADDRESSES__QUERY = gql`
  query {
    getUserShippingAddresses {
      _id
      address1
      address2
      city
      country
      provinceCode
      zip
      firstName
      lastName
      default
    }
  }
`;

export const CART__QUERY = gql`
  query {
    getCart {
      cartId
      origin
      shop {
        name
        domain
      }
      user {
        _id
        firstName
        lastName
        email
        userSince
        token
      }
      items {
        discounted_price
        gift_card
        grams
        id
        image
        key
        line_price
        original_line_price
        original_price
        price
        product_id
        quantity
        title
        total_discount
        variant_id
        vendor
      }
      updated_at
      created_at
    }
  }
`;

export const GROUPED_CART__QUERY = gql`
  query {
    getGroupedCart {
      shop {
        name
        myshopifyDomain
      }
      items {
        displayName
        id
        title
        quantity
        price
        presentmentPrices(presentmentCurrencies: [EUR, USD], first: 10) {
          edges {
            node {
              compareAtPrice {
                amount
                currencyCode
              }
              price {
                amount
                currencyCode
              }
            }
          }
        }
        product {
          images(first: 5) {
            edges {
              node {
                originalSrc
              }
            }
          }
        }
      }
    }
  }
`;

export const ORDERS__QUERY = gql`
  query {
    getOrders {
      _id
      date
      user {
        firstName
        lastName
        email
      }
      orders {
        id
        confirmed
        email
        totalPriceSet {
          presentmentMoney {
            amount
            currencyCode
          }
        }
        lineItems {
          edges {
            node {
              name
              quantity
            }
          }
        }
        shippingAddress {
          address1
          address2
        }
      }
      charge
    }
  }
`;
