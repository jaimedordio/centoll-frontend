import { gql } from "apollo-boost";

export const CART_UPDATE__SUBSCRIPTION = gql`
  subscription {
    cartUpdate {
      cartId
      origin
      shop {
        name
        domain
      }
      user {
        _id
        name
        email
        userSince
        token
      }
      items {
        discounted_price
        gift_card
        grams
        id
        image
        key
        line_price
        original_line_price
        original_price
        price
        product_id
        quantity
        title
        total_discount
        variant_id
        vendor
      }
      updated_at
      created_at
    }
  }
`;
