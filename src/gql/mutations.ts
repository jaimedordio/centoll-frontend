import { gql } from "apollo-boost";

export const LOGIN__MUTATION = gql`
  mutation ($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      _id
      firstName
      lastName
      email
      userSince
      token
    }
  }
`;

export const SIGNUP__MUTATION = gql`
  mutation (
    $firstName: String!
    $lastName: String!
    $email: String!
    $password: String!
  ) {
    signup(
      firstName: $firstName
      lastName: $lastName
      email: $email
      password: $password
    ) {
      _id
      firstName
      lastName
      email
      userSince
      token
    }

    login(email: $email, password: $password) {
      _id
      firstName
      lastName
      email
      userSince
      token
    }
  }
`;

export const VALIDATE_USER_SESSION__MUTATION = gql`
  mutation {
    validateUserSession
  }
`;

export const LOGOUT__MUTATION = gql`
  mutation {
    logout
  }
`;

export const REMOVE_CART_ITEM__MUTATION = gql`
  mutation ($cartId: ID!, $productId: ID!) {
    deleteCartItem(cartId: $cartId, productId: $productId)
  }
`;

export const CREATE_SHOPIFY_ORDER__MUTATION = gql`
  mutation ($shop: String!, $email: String!, $items: [LineItemInput!]) {
    createShopifyOrder(shop: $shop, email: $email, items: $items) {
      id
      email
      closed_at
      created_at
      updated_at
      number
      token
      total_price
      subtotal_price
      total_weight
      total_tax
      currency
      financial_status
      confirmed
      total_discounts
      total_line_items_price
      cart_token
      buyer_accepts_marketing
      name
      cancelled_at
      cancel_reason
      total_price_usd
      checkout_token
      processed_at
      app_id
      order_number
      fulfillment_status
      contact_email
      line_items {
        # discounted_price
        # gift_card
        # grams
        # id
        # image
        # key
        # line_price
        # original_line_price
        # original_price
        price
        product_id
        quantity
        title
        # total_discount
        # variant_id
        # vendor
      }
    }
  }
`;

export const CREATE_PAYMENT_SOURCE__MUTATION = gql`
  mutation ($cardToken: String!) {
    createCustomerPaymentSource(cardToken: $cardToken) {
      id
      address_city
      address_line1
      address_line1_check
      address_line2
      address_state
      address_zip
      address_zip_check
      brand
      country
      customer
      cvc_check
      dynamic_last4
      exp_month
      exp_year
      fingerprint
      funding
      last4
      name
      tokenization_method
    }
  }
`;

export const CHARGE_CUSTOMER__MUTATION = gql`
  mutation {
    chargeCustomer
  }
`;

export const CREATE_CHECKOUT__MUTATION = gql`
  mutation ($shop: String!, $items: [LineItemInput!]) {
    createCheckout(shop: $shop, items: $items) {
      token
      subtotal_price
      total_price
      total_tax
    }
  }
`;

export const CREATE_DRAFT_ORDER__MUTATION = gql`
  mutation (
    $shop: String!
    $items: [LineItemInput!]!
    $shippingAddress: ShippingAddressInput
  ) {
    createDraftOrder(
      shop: $shop
      items: $items
      shippingAddress: $shippingAddress
    )
  }
`;

export const UPDATE_CHECKOUT__MUTATION = gql`
  mutation (
    $shop: String!
    $checkoutToken: String!
    $data: shopify__CheckoutInput!
  ) {
    updateCheckout(shop: $shop, checkoutToken: $checkoutToken, data: $data) {
      token
      subtotal_price
      total_price
      total_tax
    }
  }
`;

export const COMPLETE_CHECKOUT__MUTATION = gql`
  mutation ($data: [CompleteShopCheckoutInput!]!, $paymentMethod: String!) {
    completeCheckout(data: $data, paymentMethod: $paymentMethod) {
      id
    }
  }
`;

export const UPDATE_PERSONAL_INFORMATION__MUTATION = gql`
  mutation ($newInfo: UserInput!) {
    updateUserInformation(newInfo: $newInfo) {
      _id
    }
  }
`;

export const UPDATE_PASSWORD__MUTATION = gql`
  mutation ($oldPassword: String!, $newPassword: String!) {
    updateUserPassword(oldPassword: $oldPassword, newPassword: $newPassword) {
      _id
    }
  }
`;

export const CREATE_STRIPE_SOURCE__MUTATION = gql`
  mutation ($sourceId: String!) {
    createStripeSource(sourceId: $sourceId)
  }
`;

export const CHANGE_STRIPE_DEFAULT_SOURCE__MUTATION = gql`
  mutation ($sourceId: String!) {
    changeStripeDefaultSource(sourceId: $sourceId)
  }
`;

export const DELETE_STRIPE_SOURCE__MUTATION = gql`
  mutation ($sourceId: String!) {
    deleteStripeSource(sourceId: $sourceId)
  }
`;

export const NEW_ADDRESS__MUTATION = gql`
  mutation ($shippingAddress: MailingAddressInput!) {
    addShippingAddress(shippingAddress: $shippingAddress) {
      address1
      address2
      city
      country
      province
      zip
      firstName
      lastName
      default
    }
  }
`;

export const CHANGE_DEFAULT_ADDRESS__MUTATION = gql`
  mutation ($id: ID!) {
    changeDefaultAddress(id: $id) {
      address1
      address2
      city
      country
      province
      zip
      firstName
      lastName
      default
    }
  }
`;

export const DELETE_ADDRESS__MUTATION = gql`
  mutation ($shippingAddress: ID!) {
    removeShippingAddress(shippingAddress: $shippingAddress)
  }
`;
