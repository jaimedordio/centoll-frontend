import { useCallback, useEffect, useState } from "react";
import UserServices from "../services/user-services";

// interface UseUserReturn {
//   isLogged: boolean;
//   user?: any;
// }

// export type UseUserType = () => UseUserReturn;

const useUser = () => {
  const { currentSession } = UserServices();

  const user: any = localStorage.getItem("_centoll_userId");
  const [isLogged, setIsLogged] = useState<boolean>(false);

  const validateSession = useCallback(async () => {
    const currentSessionRes = await currentSession();
    setIsLogged(currentSessionRes.data.validateUserSession);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!user) return;
    validateSession();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  return {
    user,
    isLogged,
  };
};

export default useUser;
