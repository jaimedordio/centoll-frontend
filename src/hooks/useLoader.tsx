import { useState } from "react";

const useLoader = () => {
  const [isLoading, setIsLoading] = useState(false);

  const showLoader = () => {
    setIsLoading(true);
  };

  const hideLoader = () => {
    setIsLoading(false);
  };

  const setLoader = (newState: boolean) => {
    setIsLoading(newState);
  };

  return {
    isLoading,
    showLoader,
    hideLoader,
    setLoader,
  };
};

export default useLoader;
