interface IInterfaceErrorMessage {
  [key: string]: string;
}

export const ERROR_MESSAGES: IInterfaceErrorMessage = {
  required: "Required field",
};
