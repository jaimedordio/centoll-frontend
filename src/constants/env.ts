export const environment = process.env.REACT_APP_ENV;

interface IServices {
  [key: string]: any;
  local: string;
  develop: string;
  production: string;
}

const SERVICES: IServices = {
  local: "http://localhost:8002/graphql",
  develop: "https://api.centoll.com/graphql",
  production: "https://api.centoll.com/graphql",
};

export const SERVICE_URL = SERVICES[environment as string] || SERVICES.develop;
