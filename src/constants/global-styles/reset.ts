import { createGlobalStyle } from "styled-components";
import { colors } from "../../assets/colors/colors";

export const ResetStyles = createGlobalStyle`
//   html,
//   body,
//   div,
//   span,
//   applet,
//   object,
//   iframe,
//   h1,
//   h2,
//   h3,
//   h4,
//   h5,
//   h6,
//   p,
//   blockquote,
//   pre,
//   a,
//   abbr,
//   acronym,
//   address,
//   big,
//   cite,
//   code,
//   del,
//   dfn,
//   em,
//   img,
//   ins,
//   kbd,
//   q,
//   s,
//   samp,
//   small,
//   strike,
//   strong,
//   sub,
//   sup,
//   tt,
//   var,
//   b,
//   u,
//   i,
//   center,
//   dl,
//   dt,
//   dd,
//   ol,
//   ul,
//   li,
//   fieldset,
//   form,
//   label,
//   legend,
//   table,
//   caption,
//   tbody,
//   tfoot,
//   thead,
//   tr,
//   th,
//   td,
//   article,
//   aside,
//   canvas,
//   details,
//   embed,
//   figure,
//   figcaption,
//   footer,
//   header,
//   hgroup,
//   menu,
//   nav,
//   output,
//   ruby,
//   section,
//   summary,
//   time,
//   mark,
//   audio,
//   input,
//   textarea,
//   select,
//   video {
//     margin: 0;
//     padding: 0;
//     border: 0;
//     font-size: 100%;
//     font: inherit;
//     vertical-align: baseline;
//   }

//   article,
//   aside,
//   details,
//   figcaption,
//   figure,
//   footer,
//   header,
//   hgroup,
//   menu,
//   nav,
//   section {
//     display: block;
//   }

//   body {
//     line-height: 1;
//     word-break: break-word;
//   }

//   ol,
//   ul {
//     list-style: none;
//   }

//   blockquote,
//   q {
//     quotes: none;
//   }

//   blockquote:before,
//   blockquote:after,
//   q:before,
//   q:after {
//     content: '';
//     content: none;
//   }

//   table {
//     border-collapse: collapse;
//     border-spacing: 0;
//   }

//   *, *::after, *::before {
//     box-sizing: border-box;
//   }

//   body {
//     font-family: "Inter";
//     background: #ffffff;
//     color: #333333;
//     letter-spacing: 0.005625rem;
//   }

//   a {
//     color: ${colors.brand.primary};
//     text-decoration: none;

//     &:hover {
//       text-decoration: underline;
//     }
//   }

//   img {
//     height: auto;
//     width: 100%;
//   }

//   input:not([type="range"]) {
//     -webkit-appearance: none;
//     border: none;
//     background: none;
//   }

//   input,
//   button,
//   summary {
//     &:focus {
//       outline: unset;
//     }
//   }

//   details > summary {
//     cursor: pointer;
//   }

//   input:-webkit-autofill,
//   input:-webkit-autofill:hover,
//   input:-webkit-autofill:focus,
//   input:-webkit-autofill:active {
//     transition: background-color 5000s ease-in-out 0s;
//     -webkit-text-fill-color: none !important;
//     color: inherit;
//     -webkit-box-shadow: 0 0 0 30px white inset !important;
//     box-shadow: 0 0 0 30px white inset !important;
//   }

//   button {
//     appearance: unset;
//     writing-mode:unset;
//     -webkit-writing-mode: unset;
//     text-rendering:unset;
//     color: unset;
//     letter-spacing:unset;
//     word-spacing:unset;
//     text-transform:unset;
//     text-indent:unset;
//     text-shadow:unset;
//     display:unset;
//     text-align:unset;
//     align-items: unset;
//     cursor: unset;
//     background-color: unset;
//     box-sizing: border-box;
//     margin: 0;
//     font: unset;
//     padding: unset;
//     border-width:unset;
//     border-style: unset;
//     border-color: unset;
//     border-image: unset;
//   }

//   .ckeditor {
//     h2 {
//         font-size: 26px;
//       }
//       h3 {
//         font-size: 20px;
//       }
//       h4 {
//         font-size: 18px;
//       }
//       i {
//         font-style: italic;
//       }
//       /* strong {
//         font-weight: 700;
//       } */
//       ul {
//         list-style: circle;
//         ul,
//         ol {
//           padding-left: 24px;
//         }
//       }
//       ol {
//         list-style: lower-alpha;
//         ul,
//         ol {
//           padding-left: 24px;
//         }
//       }
//       blockquote {
//         list-style: initial;
//         font-style: italic;
//         padding-left: 24px;
//         border-left: 2px solid #248489;
//       }
//       p {
//         a {
//           color: #248489;
//           text-decoration: underline;
//           /* font-weight: 700; */
//         }
//       }
//   }
`;

export default ResetStyles;
