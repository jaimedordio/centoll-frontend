import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
  @font-face {
    font-family: 'Inter';
    src: url('../../assets/fonts/Inter.ttf') format('ttf');
    font-weight: 100 1000;
  }
`;
