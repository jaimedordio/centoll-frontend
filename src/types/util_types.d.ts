export interface ILocation {
  [key: string]: any;
  state?: { orders?: IOrder[] };
}
