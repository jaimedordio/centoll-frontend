import React from "react";

interface User {
  email: string;
  firstName: string;
  lastName: string;
  token: string;
  userSince: string;
  _id: string;
}

export const UserContext = React.createContext<User | undefined>(undefined);
