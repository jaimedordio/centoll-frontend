import { ApolloProvider } from "@apollo/client";
import splitbee from "@splitbee/web";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import React, { useEffect, useState } from "react";
import { Toaster } from "react-hot-toast";
import { BrowserRouter as Router } from "react-router-dom";
import Loader from "./components/functional/loader/loader";
import { UserContext } from "./context/userContext";
// import ResetStyles from "./constants/global-styles/reset";
import useLoader from "./hooks/useLoader";
import AuthRoutes from "./routes/auth/auth.routes";
import InternalRoutes from "./routes/internal/internal.routes";
import { apolloClient } from "./utils/apollo-client";

const stripePromise = loadStripe(
  "pk_test_51Hb1IaK19feAehoJgIFiJXA1trm5PVrpH0bzKyI22ZplezcBpOEyjhNLFD9ZahPTKNjVgf948UPxkIbwQb9BCCZ300C8YkAYQb"
);

// This initiliazes Splitbee.js
splitbee.init({
  token: process.env.REACT_APP_SPLITBEE_TOKEN,
});

const App: React.FC = () => {
  const { isLoading } = useLoader();

  // const user: string | null = localStorage.getItem("_centoll_userId");
  const [user, setUser] = useState();

  useEffect(() => {
    const localUser = localStorage.getItem("centollUser");
    localUser ? setUser(JSON.parse(localUser)) : setUser(undefined);
  }, []);

  return (
    <ApolloProvider client={apolloClient}>
      <UserContext.Provider value={user}>
        <Elements stripe={stripePromise}>
          {/* <ResetStyles /> */}
          <Toaster position="top-center" />
          {isLoading ? <Loader /> : null}
          {user ? (
            <Router>
              <InternalRoutes />
            </Router>
          ) : (
            <Router>
              <AuthRoutes />
            </Router>
          )}
        </Elements>
      </UserContext.Provider>
    </ApolloProvider>
  );
};

export default App;
