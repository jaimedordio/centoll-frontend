import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";

export const LineItemStyle = styled.div`
  display: flex;
  align-items: center;
  padding: 14px 0;

  &:not(:last-child) {
    /* border-bottom: 1px solid ${colors.bn.gray_200}; */
  }

  .item-img {
    background-color: ${colors.bn.gray_200};
    box-shadow: 0px 3px 15px rgba(0, 0, 0, 0.1);
    min-width: 75px;
    max-width: 75px;
    min-height: 120px;
    max-height: 120px;
    margin-right: 20px;
    border-radius: 8px;
    overflow: hidden;
    display: flex;
    justify-content: center;
    align-items: center;

    img {
      object-fit: cover;
      height: 120px;
    }
  }

  .item-content {
    flex: 1;
    display: flex;
    justify-content: space-between;
    align-items: center;

    .item-content-left {
      display: flex;
      flex: 1;
      flex-direction: column;
      justify-content: flex-start;
      align-items: flex-start;

      &__title {
        margin-bottom: 4px;
      }

      &__subtitle {
        margin-bottom: 6px;
      }
    }
  }
`;
