import React from "react";
import { IoIosPricetag } from "react-icons/io";
import { colors } from "../../../assets/colors/colors";
import CurrencyNumber from "../../../utils/currencyNumber";
import {
  BodyDefaultBoldText,
  BodySmallText,
} from "../../styled/text/body-text";
import Button from "../button/button";
import SVGIcon from "../svg-icon/svg-icon";
import { LineItemStyle } from "./line-item.style";

interface ILineItemProps {
  cartId?: string;
  image?: string;
  onProductRemove?: (cartId: string, productId: string) => void;
  price: string;
  productId: string;
  quantity: number;
  title: string;
}

const LineItem: React.FC<ILineItemProps> = ({
  cartId,
  image,
  onProductRemove,
  price,
  productId,
  quantity,
  title,
}) => {
  return (
    <LineItemStyle>
      <div className="item-img">
        {image ? (
          <img src={image} alt="" />
        ) : (
          <SVGIcon size="24px" color={colors.bn.gray_300}>
            <IoIosPricetag />
          </SVGIcon>
        )}
      </div>
      <div className="item-content">
        <div className="item-content-left">
          <BodyDefaultBoldText className="item-content-left__title">
            {title}
          </BodyDefaultBoldText>
          <BodySmallText
            className="item-content-left__subtitle"
            color={colors.bn.gray_400}
          >
            Quantity: <span style={{ color: "black" }}>{quantity}</span>
          </BodySmallText>
          {onProductRemove && (
            <Button
              className="item-content-left__action"
              onClick={() => onProductRemove(cartId!, productId)}
              text="Remove"
              type="ghost-danger"
              size="small"
            />
          )}
        </div>
        <div className="item-content-right">
          <BodyDefaultBoldText>
            <CurrencyNumber
              locales="es-ES"
              currency="EUR"
              amount={Number(price) * quantity}
            />
          </BodyDefaultBoldText>
        </div>
      </div>
    </LineItemStyle>
  );
};

export default LineItem;
