import classnames from "classnames";
import React, { ReactNode } from "react";
import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";
import { BodySmallBoldText } from "../../styled/text/body-text";
import SVGIcon from "../svg-icon/svg-icon";

interface ITagProps {
  text: string;
  className?: string;
  icon?: ReactNode;
  type?: "error" | "warning" | "success" | "info";
  size?: "small" | "medium" | "large";
}

const TagStyle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 20px;
  width: fit-content;

  &.error {
    color: ${colors.states.standard.error};
    background-color: ${colors.lowOpacity.error};
  }

  &.warning {
    color: ${colors.states.standard.warning};
    background-color: ${colors.lowOpacity.warning};
  }

  &.success {
    color: ${colors.states.standard.success};
    background-color: ${colors.lowOpacity.success};
  }

  &.info {
    color: ${colors.brand.primary};
    background-color: ${colors.lowOpacity.primary};
  }

  &.small {
    padding: 2px 6px;

    .tag-icon {
      margin-right: 4px;
    }
  }

  &.medium {
    padding: 4px 12px;

    .tag-icon {
      margin-right: 6px;
    }
  }

  &.large {
    padding: 6px 18px;

    .tag-icon {
      margin-right: 8px;
    }
  }
`;

const Tag: React.FC<ITagProps> = ({
  text,
  className,
  icon,
  type = "info",
  size = "medium",
}) => {
  return (
    <TagStyle className={classnames(className, type, size)}>
      {icon && (
        <div className="tag-icon">
          <SVGIcon
            size={
              size === "small" ? "10px" : size === "medium" ? "12px" : "16px"
            }
            color={
              type === "error"
                ? colors.states.standard.error
                : type === "warning"
                ? colors.states.standard.warning
                : type === "success"
                ? colors.states.standard.success
                : colors.brand.primary
            }
          >
            {icon}
          </SVGIcon>
        </div>
      )}
      <BodySmallBoldText className="tag-text">{text}</BodySmallBoldText>
    </TagStyle>
  );
};

export default Tag;
