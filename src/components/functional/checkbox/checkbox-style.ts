import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";
import { BodyDefaultMixin } from "../../styled/text/body-text";

export const CheckboxStyle = styled.div`
  border-radius: 8px;
  width: fit-content;
  background-color: ${colors.bn.gray_100};
  transition: 0.3s ease-in-out all;

  .checkbox-container {
    padding: 8px 16px;
    display: flex;
    align-items: center;
    position: relative;
    cursor: pointer;
    transition: 0.3s ease-in-out all;

    &:hover {
      .checkbox-item {
        border: 2px solid ${colors.brand.primary};
      }
    }
  }

  input {
    position: absolute;
    width: 0;
    height: 0;
    opacity: 0;
    cursor: pointer;

    /* Tambien se define en active */
    &:checked ~ .checkbox-item {
      background: black;
    }
  }

  .checkbox-item {
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 14px;
    height: 14px;
    min-width: 14px;
    min-height: 14px;
    border-radius: 4px;
    border: 2px solid ${colors.bn.gray_500};
    margin-right: 16px;
    padding: 2px;
    cursor: pointer;
    transition: 0.3s ease-in-out all;

    &__icon {
      display: flex;
      align-items: center;
    }
  }

  .checkbox-label {
    ${BodyDefaultMixin};
    transition: 0.3s ease-in-out all;
    width: fit-content;

    a {
      font-weight: 500;
      cursor: pointer;
    }

    span {
      font-weight: 500;
    }
  }

  &.disabled {
    opacity: 0.4;
    pointer-events: none;

    .checkbox-item {
      border: none;
      background: ${colors.bn.gray_700};
    }

    &.radio .checkbox-item {
      border: none;
      background: ${colors.bn.gray_700};
    }
  }

  &.active {
    background-color: ${colors.bn.gray_200};

    .checkbox-container {
      .checkbox-item {
        border: 2px solid ${colors.brand.primary};
        background: ${colors.brand.primary};

        &__icon {
          display: flex;
          align-items: center;
        }
      }
    }
  }

  &.error {
    /* .checkbox-label {
      color: #ff0000;
    } */

    .checkbox-item {
      border: 1px solid #ff0000;
    }
  }

  &.radio {
    .checkbox-item {
      border-width: 1px;
      border-radius: 100%;

      &__circle  {
        width: 100%;
        background: ${colors.brand.primary};
        height: 100%;
        border-radius: 100%;
        opacity: 0;
        visibility: hidden;
        transition: 0.3s ease-in-out all;
      }
    }

    &.active {
      .checkbox-container {
        .checkbox-item {
          border: 5px solid ${colors.brand.primary};
          color: ${colors.brand.primary};
          background: ${colors.bn.white};

          &__circle {
            opacity: 0;
            visibility: visible;
          }
        }
      }
    }
  }

  &:hover {
    &.radio .checkbox-item {
      border: 1px solid ${colors.brand.primary};
      color: ${colors.brand.primary};
    }
  }

  &.indeterminated {
    .checkbox-container {
      .checkbox-item {
        border: 2px solid ${colors.brand.primary};
        background: red;
      }
    }

    &.active {
      .checkbox-container {
        .checkbox-item {
          border: 2px solid ${colors.brand.primary};
          background: ${colors.brand.primary};
        }
      }
    }
  }

  .checkbox-bottom {
    &__error {
      font-size: 12px;
      color: ${colors.states.standard.error};
    }
  }
`;
