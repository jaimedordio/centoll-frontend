import classnames from "classnames";
import React, { ReactNode, useState } from "react";
import { IoIosCheckmark } from "react-icons/io";
import { colors } from "../../../assets/colors/colors";
import SVGIcon from "../svg-icon/svg-icon";
import { CheckboxStyle } from "./checkbox-style";

interface ICheckboxProps {
  checkColor?: string;
  checked?: boolean;
  children?: ReactNode;
  className?: string;
  disabled?: boolean;
  error?: boolean;
  errorMessage?: string;
  icon?: string;
  indeterminated?: boolean;
  inputRef?: any;
  isRadio?: boolean;
  message?: string;
  name?: string;
  onBlur?: (ev: React.FocusEvent<HTMLInputElement>) => void;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onFocus?: (e: React.FocusEvent<HTMLInputElement>) => void;
  onKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
  required?: boolean;
  tooltip?: string;
}

const Checkbox: React.FC<ICheckboxProps> = (props) => {
  const [checked, setChecked] = useState(false);

  const handleChange = (ev: React.ChangeEvent<HTMLInputElement>) => {
    if (props.onChange) {
      props.onChange(ev);
    } else if (props.checked === undefined) {
      setChecked(ev.target.checked);
    }
  };

  const check = props.checked || checked;

  return (
    <CheckboxStyle
      className={classnames(props.className || "", {
        error: !!props.error,
        disabled: !!props.disabled,
        required: !!props.required,
        active: !!check,
        radio: props.isRadio,
        indeterminated: props.indeterminated,
      })}
    >
      <label className="checkbox-container">
        <input
          type="checkbox"
          ref={props.inputRef}
          name={props.name}
          disabled={props.disabled}
          required={props.required}
          checked={props.checked || checked}
          onChange={(ev) => !props.disabled && handleChange(ev)}
          onFocus={(ev: React.FocusEvent<HTMLInputElement>) =>
            !props.disabled && props.onFocus && props.onFocus(ev)
          }
          onBlur={(ev: React.FocusEvent<HTMLInputElement>) =>
            !props.disabled && props.onBlur && props.onBlur(ev)
          }
          onKeyDown={(ev: React.KeyboardEvent<HTMLInputElement>) =>
            !props.disabled && props.onKeyDown && props.onKeyDown(ev)
          }
        />
        <div className="checkbox-item">
          {!props.isRadio ? (
            <div className="checkbox-item__icon">
              <SVGIcon
                color={
                  check
                    ? props.checkColor
                      ? props.checkColor
                      : colors.bn.white
                    : "transparent"
                }
                size="16px"
              >
                <IoIosCheckmark />
              </SVGIcon>
            </div>
          ) : (
            <div className="checkbox-item__circle" />
          )}
        </div>
        {props.children && (
          <div className="checkbox-label">{props.children}</div>
        )}
        <div className="checkbox-bottom">
          {props.errorMessage && (
            <div className="checkbox-bottom-error">
              <p>{props.errorMessage}</p>
            </div>
          )}
          {props.message && (
            <div className="checkbox-bottom-message">
              <p>{props.message}</p>
            </div>
          )}
        </div>
      </label>
    </CheckboxStyle>
  );
};

export default Checkbox;
