import React from "react";
import styled, { css } from "styled-components";

interface ISVGIcon {
  children: React.ReactNode;
  color?: string;
  strokeColor?: string;
  size?: string;
  className?: string;
}

interface ISVGIconStyle {
  color?: string;
  strokeColor?: string;
  size?: string;
}

const SVGIconStyle = styled.div<ISVGIconStyle>`
  display: flex;
  justify-content: center;
  align-items: center;

  svg {
    width: 100%;
    height: auto;
  }

  ${(props) =>
    props.color &&
    css`
      path {
        fill: ${props.color};
      }
    `}

  ${(props) =>
    props.strokeColor &&
    css`
      path {
        stroke: ${props.strokeColor};
      }
    `}

	${(props) =>
    props.size &&
    css`
      width: ${props.size};
      min-width: ${props.size};
      height: ${props.size};

      svg {
        width: ${props.size};
        min-width: ${props.size};
        height: ${props.size};
      }
    `}
`;

const SVGIcon: React.FC<ISVGIcon> = ({
  children,
  color,
  strokeColor,
  size = "20px",
  className,
}) => {
  return (
    <SVGIconStyle
      className={className}
      color={color}
      size={size}
      strokeColor={strokeColor}
    >
      {/* <ReactSVG src={icon} /> */}
      {children}
    </SVGIconStyle>
  );
};

export default SVGIcon;
