import { ReactNode } from "react";
import { IoIosArrowDown, IoIosPerson } from "react-icons/io";
import { colors } from "../../../assets/colors/colors";
import { BodySmallBoldText } from "../../styled/text/body-text";
import SVGIcon from "../svg-icon/svg-icon";
import { AvatarStyle } from "./avatar-style";

interface IAvatarProps {
  rounded?: boolean;
  image?: string;
  alert?: {
    type: "error" | "warning";
    message: ReactNode;
  };
  user?: string;
  dropdownArrow?: boolean;
}

const Avatar: React.FC<IAvatarProps> = ({
  rounded,
  image,
  alert,
  user,
  dropdownArrow,
}) => {
  return (
    <AvatarStyle
      rounded={rounded}
      alertColor={
        alert?.type === "error"
          ? colors.states.standard.error
          : colors.states.standard.warning
      }
    >
      <div className="avatar-image">
        <div className="avatar-image-img">
          {image ? (
            <img src={image} alt="" />
          ) : (
            <SVGIcon size="14px" color={colors.brand.tertiary}>
              <IoIosPerson />
            </SVGIcon>
          )}
        </div>
        {alert && <div className="avatar-image-alert " />}
      </div>
      {user && (
        <div className="avatar-name">
          <BodySmallBoldText>{user}</BodySmallBoldText>
        </div>
      )}
      {dropdownArrow && (
        <div className="avatar-arrow">
          <SVGIcon size="16px">
            <IoIosArrowDown />
          </SVGIcon>
        </div>
      )}
    </AvatarStyle>
  );
};

export default Avatar;
