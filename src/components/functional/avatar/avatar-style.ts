import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";

interface IAvatarStyleProps {
  rounded?: boolean;
  alertColor?: string;
}

export const AvatarStyle = styled.div<IAvatarStyleProps>`
  display: flex;
  justify-content: flex-start;
  align-items: center;

  .avatar-image {
    position: relative;
    height: 28px;
    width: 28px;

    .avatar-image-img {
      overflow: hidden;
      background-color: ${colors.lowOpacity.tertiary};
      border-radius: ${(props) => (props.rounded ? "100%" : "0")};
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .avatar-image-alert {
      position: absolute;
      top: -2px;
      right: -2px;
      height: 10px;
      width: 10px;
      border-radius: 100%;
      background-color: ${(props) => props.alertColor};
    }
  }

  .avatar-name {
    margin-left: 12px;
    flex: 1;
  }

  .avatar-arrow {
    margin-left: 4px;
  }
`;
