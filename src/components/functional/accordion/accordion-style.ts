import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";

export const AccordionStyle = styled.details`
  background-color: ${colors.bn.gray_100};
  border: 1px solid ${colors.bn.gray_200};
  padding: 12px;
  border-radius: 9px;

  .accordion-summary {
  }

  .accordion-detail {
    border-top: 1px solid ${colors.bn.gray_200};
    padding-top: 20px;
    margin-top: 12px;
  }

  & > summary {
    list-style-type: none;

    &::-webkit-details-marker {
      display: none;
    }

    &::before {
      content: "▶";
      margin-right: 8px;
    }
  }

  &[open] > summary::before {
    transform: rotate(90deg);
  }
`;
