import { ReactNode } from "react";
import { AccordionStyle } from "./accordion-style";

interface IAccordionProps {
  summary: { content: ReactNode; className?: string };
  detail: { content: ReactNode; className?: string };
  open?: boolean;
  className?: string;
}

const Accordion: React.FC<IAccordionProps> = ({
  summary,
  detail,
  open,
  className,
}) => {
  return (
    <AccordionStyle open={open} className={className}>
      <summary className={`accordion-summary ${summary.className}`}>
        {summary.content}
      </summary>
      <div className={`accordion-detail ${detail.className}`}>
        {detail.content}
      </div>
    </AccordionStyle>
  );
};

export default Accordion;
