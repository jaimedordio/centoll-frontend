import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";

export const AlertStyle = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  border-radius: 12px;
  padding: 20px;

  &.success {
    background-color: ${colors.lowOpacity.success};
  }

  .alert-icon {
    margin-right: 12px;
    background-color: ${colors.lowOpacity.success};
    border-radius: 100%;
    padding: 4px;
  }

  .alert-content {
    flex: 1;
    padding: 4px 0;
    color: ${colors.states.standard.success};
  }
`;
