import { IoIosCheckmark } from "react-icons/io";
import { colors } from "../../../assets/colors/colors";
import { BodyDefaultText } from "../../styled/text/body-text";
import SVGIcon from "../svg-icon/svg-icon";
import { AlertStyle } from "./alert-style";

const Alert: React.FC = () => {
  return (
    <AlertStyle className="success">
      <div className="alert-icon">
        <SVGIcon size="24px" color={colors.states.standard.success}>
          <IoIosCheckmark />
        </SVGIcon>
      </div>
      <div className="alert-content">
        <BodyDefaultText className="alert-content__text">Alert</BodyDefaultText>
      </div>
    </AlertStyle>
  );
};

export default Alert;
