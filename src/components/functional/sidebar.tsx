import React from "react";

const Sidebar = (props: any) => {
  return (
    <div className="flex w-full max-w-xs p-4 text-sm font-medium rounded-lg h-fit bg-gray-50">
      <ul className="flex flex-col w-full">
        <li className="my-px">
          <a
            href="#"
            className="flex flex-row items-center h-12 px-4 text-gray-600 bg-gray-100 rounded-lg"
          >
            <span className="flex items-center justify-center text-lg text-gray-400">
              <svg
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                viewBox="0 0 24 24"
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path>
              </svg>
            </span>
            <span className="ml-3">Dashboard</span>
            <span className="flex items-center justify-center h-6 px-2 ml-auto text-sm font-semibold text-gray-500 bg-gray-200 rounded-full">
              3
            </span>
          </a>
        </li>
        <li className="my-px">
          <span className="flex px-4 my-4 text-sm font-medium text-gray-400 uppercase">
            Account
          </span>
        </li>
        <li className="my-px">
          <a
            href="#"
            className="flex flex-row items-center h-12 px-4 text-gray-600 rounded-lg hover:bg-gray-100"
          >
            <span className="flex items-center justify-center text-lg text-gray-400">
              <svg
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                viewBox="0 0 24 24"
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"></path>
              </svg>
            </span>
            <span className="ml-3">Profile</span>
          </a>
        </li>
        <li className="my-px">
          <a
            href="#"
            className="flex flex-row items-center h-12 px-4 text-gray-600 rounded-lg hover:bg-gray-100"
          >
            <span className="flex items-center justify-center text-lg text-gray-400">
              <svg
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                viewBox="0 0 24 24"
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"></path>
              </svg>
            </span>
            <span className="ml-3">Notifications</span>
            <span className="flex items-center justify-center h-6 px-2 ml-auto text-sm font-semibold text-gray-500 bg-gray-200 rounded-full">
              10
            </span>
          </a>
        </li>
        <li className="my-px">
          <a
            href="#"
            className="flex flex-row items-center h-12 px-4 text-gray-600 rounded-lg hover:bg-gray-100"
          >
            <span className="flex items-center justify-center text-lg text-gray-400">
              <svg
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                viewBox="0 0 24 24"
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path>
                <path d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path>
              </svg>
            </span>
            <span className="ml-3">Settings</span>
          </a>
        </li>
        <li className="my-px">
          <a
            href="#"
            className="flex flex-row items-center h-12 px-4 text-gray-600 rounded-lg hover:bg-gray-100"
          >
            <span className="flex items-center justify-center text-lg text-red-400">
              <svg
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                viewBox="0 0 24 24"
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path d="M8 11V7a4 4 0 118 0m-4 8v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2z"></path>
              </svg>
            </span>
            <span className="ml-3">Logout</span>
          </a>
        </li>
      </ul>
    </div>
  );
};

export default Sidebar;
