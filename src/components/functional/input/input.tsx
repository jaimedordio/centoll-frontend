import classnames from "classnames";
import { ReactNode, useState } from "react";
import {
  IoIosCheckmarkCircle,
  IoIosCloseCircle,
  IoIosInformationCircleOutline,
} from "react-icons/io";
import Select from "react-select";
import ReactTooltip from "react-tooltip";
import { colors } from "../../../assets/colors/colors";
import { ERROR_MESSAGES } from "../../../constants/errorMessages";
import { BodySmallText } from "../../styled/text/body-text";
import SVGIcon from "../svg-icon/svg-icon";
import { InputStyle } from "./input.style";

interface IInputProps {
  className?: string;
  disabled?: boolean;
  errorMessage?: string;
  iconCustomErrorMessage?: string;
  iconCustomSuccessMessage?: string;
  iconErrorMessage?: boolean;
  iconLeft?: {
    icon: ReactNode;
    function?: any;
  };
  iconRight?: {
    icon: ReactNode;
    function?: any;
  };
  iconSuccessMessage?: boolean;
  initialValue?: string;
  inputRef?: any;
  label?: string;
  maxLength?: number;
  message?: string;
  name?: string;
  onBlur?: any;
  onChange?: any;
  onEnterKey?: any;
  onFocus?: any;
  options?: { value: string; label: string }[];
  pattern?: string;
  placeholder?: string;
  readOnly?: boolean;
  required?: boolean;
  select?: boolean;
  size?: "small" | "medium" | "large";
  success?: boolean;
  successMessage?: string;
  textarea?: boolean;
  tooltip?: string;
  topMessage?: any;
  type?: "text" | "password" | "number";
  value?: string;
}

const Input: React.FC<IInputProps> = (props: IInputProps) => {
  const [focus, setFocus] = useState(false);

  const errorText = props.errorMessage
    ? ERROR_MESSAGES[props.errorMessage]
    : "";

  const handleChange = (
    ev:
      | React.ChangeEvent<HTMLInputElement>
      | React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    console.log("handleChange ev", ev.currentTarget);
    if (!props.disabled) {
      if (props.onChange) {
        props.onChange(ev);
      }
    }
  };

  const handleFocus = (
    ev:
      | React.FocusEvent<HTMLInputElement>
      | React.FocusEvent<HTMLTextAreaElement>
  ) => {
    if (props.disabled) {
      return;
    } else {
      setFocus(true);
      return props.onFocus && props.onFocus(ev);
    }
  };

  const handleBlur = (
    ev:
      | React.FocusEvent<HTMLInputElement>
      | React.FocusEvent<HTMLTextAreaElement>
  ) => {
    if (props.disabled) {
      return;
    } else {
      setFocus(false);
      return props.onBlur && props.onBlur(ev);
    }
  };

  return (
    <InputStyle
      className={classnames(props.className || "", props.size || "small", {
        focus,
        success: Boolean(props.success),
        error: Boolean(props.errorMessage),
        disabled: Boolean(props.disabled),
        required: Boolean(props.required),
        complete: props.value && props.value.length > 0,
      })}
    >
      {(props.label || props.tooltip) && (
        <div className="input-top">
          {props.label && (
            <div className="input-top-label">
              <>
                {props.required && "*"} {props.label}
              </>
            </div>
          )}
          {props.tooltip && (
            <>
              <ReactTooltip />
              <div data-tip={props.tooltip} className="input-top-tooltip">
                <SVGIcon color={colors.bn.gray_700}>
                  {IoIosInformationCircleOutline}
                </SVGIcon>
              </div>
            </>
          )}
        </div>
      )}

      {props.select ? (
        <div className="input-select">
          <Select
            className="react-select-container"
            classNamePrefix="react-select"
            placeholder={props.placeholder}
            options={props.options}
          />
        </div>
      ) : (
        <div className="input-body">
          {props.iconLeft && (
            <div className="input-body-icon left">
              <SVGIcon color={colors.bn.gray_100}>
                {props.iconLeft.icon}
              </SVGIcon>
            </div>
          )}

          {props.textarea ? (
            <textarea
              name={props.name}
              placeholder={props.placeholder}
              value={props.value}
              disabled={props.disabled}
              required={props.required}
              onChange={(ev) => handleChange(ev)}
              onFocus={(ev) => handleFocus(ev)}
              onBlur={(ev) => handleBlur(ev)}
              ref={props.inputRef}
            />
          ) : (
            <input
              type={props.type}
              name={props.name}
              placeholder={props.placeholder}
              value={props.value}
              disabled={props.disabled}
              readOnly={props.readOnly}
              required={props.required}
              onChange={(ev) => handleChange(ev)}
              onFocus={(ev) => handleFocus(ev)}
              onBlur={(ev) => handleBlur(ev)}
              ref={props.inputRef}
            />
          )}

          {props.iconRight && (
            <div className="input-body-icon left">
              <SVGIcon color={colors.bn.gray_300}>
                {props.iconRight.icon}
              </SVGIcon>
            </div>
          )}
        </div>
      )}

      <div className="input-bottom">
        {props.errorMessage && (
          <div className="input-bottom-validation">
            {props.iconErrorMessage ||
              (props.iconCustomErrorMessage && (
                <div className="input-bottom-validation__icon">
                  <SVGIcon size="16px" color={colors.states.standard.error}>
                    {props.iconCustomErrorMessage || <IoIosCloseCircle />}
                  </SVGIcon>
                </div>
              ))}
            <div className="input-bottom-validation__text">
              <BodySmallText>{props.errorMessage && errorText}</BodySmallText>
            </div>
          </div>
        )}
        {props.successMessage && (
          <div className="input-bottom-validation">
            {props.iconSuccessMessage ||
              (props.iconCustomSuccessMessage && (
                <div className="input-bottom-validation__icon">
                  <SVGIcon size="16px" color={colors.states.standard.success}>
                    {props.iconCustomSuccessMessage || <IoIosCheckmarkCircle />}
                  </SVGIcon>
                </div>
              ))}
            <div className="input-bottom-validation__text">
              <BodySmallText>{props.successMessage}</BodySmallText>
            </div>
          </div>
        )}

        {props.message && (
          <div className="input-bottom-message">
            <BodySmallText>{props.message}</BodySmallText>
          </div>
        )}
      </div>
    </InputStyle>
  );
};

export default Input;
