import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";
import { BodySmallMixin } from "../../styled/text/body-text";

export const InputStyle = styled.div`
  position: relative;
  margin-bottom: 20px;

  /* TOP */
  .input-top {
    display: flex;
    align-items: center;
    margin-bottom: 4px;

    .input-top-label {
      flex: auto;
      color: ${colors.bn.gray_700};
      ${BodySmallMixin};
    }

    .input-top-tooltip {
      width: 13px;
      height: 13px;
      margin-left: 8px;
      cursor: pointer;
    }
  }

  /* BODY */
  .input-body {
    position: relative;
    display: flex;
    border: 1px solid ${colors.bn.gray_300};
    border-radius: 12px;
    margin-bottom: 4px;
    transition: 0.3s ease-in-out all;

    input {
      flex: auto;
      font-size: 13px;
      line-height: 22px;
      font-weight: 400;
      color: ${colors.bn.gray_700};
      width: 100%;
      padding: 0;
      background: transparent;
      border: none;

      &::placeholder {
        color: ${colors.bn.gray_300};
      }
    }

    textarea {
      font-size: 14px;
      font-weight: 400;
      color: ${colors.bn.gray_700};
      width: 100%;
      padding: 0;
      background: none;
      border: none;
      resize: none;

      &::placeholder {
        color: ${colors.bn.gray_300};
      }
    }

    .input-body-icon {
      width: 16px;
      height: 16px;
      min-width: 16px;
      min-height: 16px;
      margin-left: 8px;
      align-self: center;
      cursor: pointer;
      display: flex;
      align-items: center;

      &.left {
        margin-left: 0px;
        margin-right: 8px;
      }
    }
  }

  /* BOTTOM */
  .input-bottom {
    .input-bottom-validation {
      display: flex;

      &__text {
        color: ${colors.states.standard.error};
      }

      &__icon {
        margin-right: 8px;
      }
    }
  }

  /* STATES */
  &:hover {
    .input-body {
      border: 1px solid ${colors.brand.primary};
    }

    .input-body-icon path {
      fill: ${colors.brand.primary};
    }
  }

  &.focus {
    .input-body {
      border: 1px solid ${colors.brand.primary};
    }

    .input-body-icon path {
      fill: ${colors.brand.primary};
    }
  }

  &.error {
    .input-body {
      border: 1px solid ${colors.states.standard.error};
    }

    .input-bottom-error {
      color: ${colors.states.standard.error};
    }
  }

  &.disabled {
    opacity: 0.4;

    .input-top-label {
      color: ${colors.bn.gray_400};
    }

    .input-body {
      border: 1px solid ${colors.bn.gray_400};
      background: ${colors.bn.gray_200};

      input {
        user-select: none;
      }
    }
  }

  &.success {
    .input-body {
      border: 1px solid ${colors.states.standard.success};
    }

    .input-bottom-validation {
      color: ${colors.states.standard.success};
    }
  }

  /* SIZES */
  &.small  {
    .input-top {
      .input-top-label {
        font-size: 13px;
      }
    }

    .input-body {
      padding: 6px 16px;
      border-radius: 8px;

      input {
        font-size: 13px;
      }
    }
  }

  &.medium {
    .input-body {
      padding: 8px 16px;

      input {
        font-size: 14px;
      }
    }
  }

  &.large {
    .input-top {
      .input-top-label {
        font-weight: 500;
      }
    }

    .input-body {
      padding: 10px 16px;

      input {
        font-size: 15px;
      }
    }
  }

  /* SELECT */
  .input-select {
    .react-select {
      &__placeholder {
        font-size: 12px;
        color: ${colors.bn.gray_200};
      }

      &__control {
        background-color: #333333;
        border: 1px solid #333333;
        border-radius: 0;
        min-height: unset;
      }

      &__value-container {
        padding: 2px 12px;
      }

      &__single-value {
        position: relative;
        top: unset;
        transform: unset;
        color: #ffffff;
        font-size: 14px;
        line-height: 22px;
      }

      &__indicator-separator {
        margin-bottom: 10px;
        margin-top: 10px;
        display: none;
      }

      &__indicator {
        padding: 6px;

        svg {
          height: 15px;
          width: 15px;
        }
      }

      &__menu {
        background-color: #333333;
        border-radius: 0;
      }

      &__option {
        font-size: 14px;
      }

      &__option--is-focused {
        background-color: #252525;
      }

      &__option--is-selected {
        background-color: ${colors.brand.secondary};
      }
    }
  }
`;
