import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";

export const InputFileStyle = styled.div`
  .brainhub-file-input__wrapper {
    width: 100%;
  }

  .brainhub-file-input__label {
    flex: auto;
    font-size: 13px;
    line-height: 13px;
    color: ${colors.bn.gray_700};
    margin-bottom: 8px;
  }

  .brainhub-drop-area {
    align-items: center;
    justify-content: center;
    background-color: #333333;
    border: 1px solid #333333;
    border-radius: 0;
    font-family: unset;
    height: 130px;

    &__button  {
      font-size: 15px;
      color: #ffffff;
    }
  }

  .brainhub-image-rotator {
    margin-bottom: 12px;

    &__controls {
      display: flex;
      justify-content: center;
      align-items: center;

      .brainhub-image-rotator__button--save {
        background-color: ${colors.brand.secondary};
      }

      .brainhub-image-rotator__button--arrow {
        display: flex;
        justify-content: center;
        align-items: center;

        svg {
          height: 0.8em !important;
        }
      }
    }

    &__button {
      border-radius: 4px;
      margin-top: 0;
      margin-bottom: 0;
      max-height: 24px;
      min-height: 24px;
      cursor: pointer;
    }
  }

  .brainhub-file-info {
    margin: 20px 0;

    &__metadata {
      font-size: 14px;
      margin: 0 0 0 12px;

      &__info {
        margin: 0;
        display: flex;
        align-items: center;
      }
    }
  }
`;
