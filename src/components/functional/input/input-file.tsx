import { InputFileStyle } from "./input-file.style";
const FileInput = require("@brainhubeu/react-file-input"); // use require because of missing TS types

interface IInputFileProps {
  className?: string;
  dropAreaClassName?: string;
  fileInfoClassName?: string;
  imageEditorClassName?: string;
  dragOnDocument?: boolean;
  dropOnDocument?: boolean;
  label: string;
  metadataComponent?: React.FC;
  thumbnailComponent?: React.FC;
  displayImageThumbnail?: boolean;
  cropAspectRatio?: number;
  cropTool?: boolean;
  scaleOptions?: { width: number; height: number; keepAspectRatio: boolean };
  onChangeCallback?: () => void;
  onDragEnterCallback?: () => void;
  onDragLeaveCallback?: () => void;
}

const InputFile: React.FC<IInputFileProps> = ({
  className,
  dropAreaClassName,
  fileInfoClassName,
  imageEditorClassName,
  dragOnDocument,
  dropOnDocument,
  label,
  metadataComponent,
  thumbnailComponent,
  displayImageThumbnail,
  cropAspectRatio,
  cropTool,
  scaleOptions,
  onChangeCallback,
  onDragEnterCallback,
  onDragLeaveCallback,
}) => {
  return (
    <InputFileStyle>
      <FileInput
        className={className}
        dropAreaClassName={dropAreaClassName}
        fileInfoClassName={fileInfoClassName}
        imageEditorClassName={imageEditorClassName}
        dragOnDocument={dragOnDocument}
        dropOnDocument={dropOnDocument}
        label={label}
        metadataComponent={metadataComponent}
        thumbnailComponent={thumbnailComponent}
        displayImageThumbnail={displayImageThumbnail}
        cropAspectRatio={cropAspectRatio}
        cropTool={cropTool}
        scaleOptions={scaleOptions}
        onChangeCallback={onChangeCallback}
        onDragEnterCallback={onDragEnterCallback}
        onDragLeaveCallback={onDragLeaveCallback}
      />
    </InputFileStyle>
  );
};

export default InputFile;
