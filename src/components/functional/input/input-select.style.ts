import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";
import { BodySmallMixin } from "../../styled/text/body-text";

export const InputSelectStyle = styled.div`
  position: relative;

  /* TOP */
  .input-top {
    display: flex;
    align-items: center;
    margin-bottom: 4px;

    .input-top-label {
      flex: auto;
      color: ${colors.bn.gray_700};
      ${BodySmallMixin};
    }

    .input-top-tooltip {
      width: 13px;
      height: 13px;
      margin-left: 8px;
      cursor: pointer;
    }
  }

  /* BOTTOM */
  .input-bottom {
    .input-bottom-validation {
      display: flex;

      &__text {
        color: ${colors.states.standard.error};
      }

      &__icon {
        margin-right: 8px;
      }
    }
  }

  /* STATES */
  &:hover {
    .input-body {
      border: 1px solid ${colors.brand.primary};
    }

    .input-body-icon path {
      fill: ${colors.brand.primary};
    }
  }

  &.focus {
    .input-body {
      border: 1px solid ${colors.brand.primary};
    }

    .input-body-icon path {
      fill: ${colors.brand.primary};
    }
  }

  &.error {
    .input-bottom-error {
      color: ${colors.states.standard.error};
    }
  }

  &.disabled {
    opacity: 0.4;

    .input-top-label {
      color: ${colors.bn.gray_400};
    }

    .input-body {
      border: 1px solid ${colors.bn.gray_400};
      background: ${colors.bn.gray_200};

      input {
        user-select: none;
      }
    }
  }

  &.success {
    .input-body {
      border: 1px solid ${colors.states.standard.success};
    }

    .input-bottom-validation {
      color: ${colors.states.standard.success};
    }
  }

  /* SIZES */
  &.small  {
    .input-top {
      .input-top-label {
        font-size: 13px;
      }
    }
  }

  &.large {
    .input-top {
      .input-top-label {
        font-weight: 500;
      }
    }
  }

  /* SELECT */
  .input-select {
    height: 36px;
    min-height: 36px;

    .react-select {
      &__placeholder {
        font-size: 14px;
        color: ${colors.bn.gray_600};
        position: relative;
        transform: none;
      }

      &__control {
        background-color: #ffffff;
        box-shadow: 0 1px 3px 0 rgb(0 0 0 / 10%), 0 1px 2px 0 rgb(0 0 0 / 6%);
        border-radius: 8px;
        min-height: unset;
        min-width: 100%;
        cursor: pointer;
      }

      &__value-container {
        padding: 2px 16px 2px 6px !important;
      }

      &__single-value {
        font-size: 14px;
        line-height: 21px;
        position: relative;
        transform: none;
        overflow: unset;
        text-overflow: unset;
      }

      &__indicator-separator {
        margin-bottom: 10px;
        margin-top: 10px;
        display: none;
      }

      &__indicator {
        padding: 4px;

        svg {
          height: 15px;
          width: 15px;
        }
      }

      &__menu {
        background-color: #ffffff;
        border-radius: 8px;
        overflow: hidden;

        &-list {
          padding: 0;
        }
      }

      &__option {
        font-size: 14px;
        color: ${colors.bn.gray_600};
      }

      &__option--is-focused {
        background-color: ${colors.bn.gray_100};
      }

      &__option--is-selected {
        background-color: ${colors.lowOpacity.primary};
        color: ${colors.bn.gray_600};
      }
    }
  }
`;
