import React from "react";
import LoaderContainer from "./loader.style";

interface LoaderProps {
  className?: string;
}

const Loader: React.FC<LoaderProps> = (props: LoaderProps) => {
  return (
    <LoaderContainer>
      <div className="lodaer-container">
        <div className="loader-container-spinner" />
      </div>
    </LoaderContainer>
  );
};

export default Loader;
