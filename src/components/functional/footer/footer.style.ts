import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";

export const FooterStyle = styled.div`
  background: ${colors.bn.white};
  padding: 20px 0;
  border-top: 1px solid rgba(0, 0, 0, 0.11);

  .footer-container {
    display: flex;
    justify-content: space-between;
    align-items: center;

    .footer-container-left {
      img {
        width: 120px;
      }
    }

    .footer-container-right {
      display: flex;
      justify-content: flex-end;
      align-items: center;

      &__link {
        text-decoration: underline;

        &:not(:first-child) {
          margin-left: 20px;
        }
      }
    }
  }
`;
