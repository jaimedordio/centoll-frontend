import React from "react";
import { NavLink } from "react-router-dom";
import { images } from "../../../assets/images/images";
import { FooterStyle } from "./footer.style";

const Footer = () => {
  return (
    <FooterStyle>
      <div className="container footer-container">
        <div className="footer-container-left">
          <img src={images.logo} alt="" />
        </div>
        <div className="footer-container-right">
          <NavLink
            className="footer-container-right__link"
            to="terms-of-service"
          >
            Terms of Service
          </NavLink>
          <NavLink className="footer-container-right__link" to="privacy-policy">
            Privacy Policy
          </NavLink>
        </div>
      </div>
    </FooterStyle>
  );
};

export default Footer;
