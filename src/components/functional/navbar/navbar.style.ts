import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";
import { BodySmallBoldMixin } from "../../styled/text/body-text";
import { LinkMixin } from "../../styled/text/other-text";

export const NavbarStyle = styled.div`
  position: relative;
  background: ${colors.bn.white};
  height: 66px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.11);

  & a {
    ${LinkMixin};
    text-decoration: none;
    color: ${colors.bn.gray_700};

    &:hover {
      text-decoration: none;
    }
  }

  .navbar-container {
    height: 100%;
    display: flex;
    justify-content: flex-start;
    align-items: center;

    .navbar-container-logo {
      width: 110px;
      margin-right: 20px;

      a {
        display: flex;
        align-items: center;
      }
    }

    .navbar-container-items,
    .navbar-container-right {
      &-item {
        margin: 0 8px;
        font-size: 0.95rem;
        font-weight: 600;
        letter-spacing: 0.02rem;

        &.active {
          font-weight: 700;
          color: ${colors.brand.primary};
        }
      }
    }

    .navbar-container-items {
      margin-right: auto;
      display: flex;
      justify-content: flex-start;
      align-items: center;
    }

    .navbar-container-right {
      display: flex;
      justify-content: flex-end;
      align-items: center;

      .navbar-container-right-item {
        .navbar-container-right-item-user {
          margin-left: 12px;
          position: relative;
          display: flex;
          align-items: center;
          cursor: pointer;

          &__dropdown {
            visibility: hidden;
            opacity: 0;
            position: absolute;
            top: 120%;
            right: 0;
            width: max-content;
            border: 1px solid ${colors.bn.gray_200};
            border-radius: 8px;
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            background-color: ${colors.bn.white};
            transition: 0.3s ease-in-out all;

            &.active {
              visibility: visible;
              opacity: 1;
            }

            &__item {
              ${BodySmallBoldMixin};
              padding: 8px 24px;
              transition: 0.3s ease-in-out all;

              &:not(:last-child) {
                border-bottom: 1px solid ${colors.bn.gray_200};
              }

              &:hover {
                background-color: ${colors.bn.gray_100};
              }

              &.logout {
                line-height: 24px;
                color: ${colors.states.standard.error};
              }
            }
          }
        }
      }
    }

    .navbar-container-drop-action {
      margin-left: auto;
      display: none;
    }
  }

  .navbar-drop {
    display: none;
  }

  @media (max-width: 1100px) {
    .navbar-drop {
      display: block;
      position: absolute;
      top: calc(100% + 1px);
      width: 100%;
      height: 0;
      padding: 0 20px;
      background-color: ${colors.bn.white};
      border-bottom: 1px solid rgba(0, 0, 0, 0.11);
      overflow-y: auto;
      transition: 0.3s ease-in-out all;

      &.active {
        padding: 20px;
        height: 187px;
      }

      .navbar-drop-links,
      .navbar-drop-bottom {
        &-item {
          padding-bottom: 8px;
          font-size: 0.95rem;
          font-weight: 600;
          letter-spacing: 0.02rem;

          & a.active {
            font-weight: 700;
            color: ${colors.brand.primary};
          }
        }
      }

      .navbar-drop-user {
        padding: 10px 0;
        border-top: 1px solid rgba(0, 0, 0, 0.09);
        border-bottom: 1px solid rgba(0, 0, 0, 0.09);

        &__dropdown {
          max-height: 0;
          overflow-y: auto;
          transition: 0.3s ease-in-out all;

          &.active {
            max-height: 700px;
          }

          &__item {
            display: block;
            font-size: 0.9rem;
            font-weight: 500;
            letter-spacing: 0.02rem;
            margin: 8px 0;

            &.logout {
              color: ${colors.states.standard.error};
            }
          }
        }
      }

      .navbar-drop-bottom {
        &-item {
          padding-top: 8px;

          &:last-child {
            padding-bottom: 0;
          }
        }
      }
    }

    .navbar-container {
      .navbar-container-items,
      .navbar-container-right {
        display: none;
      }

      .navbar-container-drop-action {
        display: block;
      }
    }
  }

  @media (max-width: 768px) {
  }
`;
