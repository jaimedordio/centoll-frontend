import {
  Avatar,
  Box,
  Flex,
  HStack,
  Icon,
  Image,
  Menu,
  MenuButton,
  MenuDivider,
  MenuItem,
  MenuList,
  Text,
} from "@chakra-ui/react";
import gravatar from "gravatar";
import React, { useContext, useEffect, useState } from "react";
import { IoChevronDownOutline } from "react-icons/io5";
import { NavLink, useHistory } from "react-router-dom";
import { colors } from "../../../assets/colors/colors";
import { images } from "../../../assets/images/images";
import { UserContext } from "../../../context/userContext";
import useUser from "../../../hooks/useUser";
import UserServices from "../../../services/user-services";

const routes: { text: string; path: string }[] = [
  { text: "Checkout", path: "/checkout" },
  { text: "Orders", path: "/order-history" },
];

const dropdownRoutes: {
  text: string;
  path: string;
  icon?: React.ReactElement;
}[] = [{ text: "My profile", path: "/profile", icon: <></> }];

const NavBar = () => {
  const history = useHistory();
  const user = useContext(UserContext);

  const { isLogged } = useUser();
  const { logout } = UserServices();

  useEffect(() => {
    setResNavbarState(false);
  }, [history.location.pathname]);

  const [userDropdownState, setUserDropdownState] = useState(false);
  const [resNavbarState, setResNavbarState] = useState(false);

  return (
    <Box
      bg="white"
      borderBottomColor="gray.100"
      borderBottomWidth={1}
      as="header"
    >
      <Flex
        h={16}
        alignItems={"center"}
        justifyContent={"space-between"}
        margin="0 auto"
        className="container"
      >
        <HStack spacing={8} alignItems={"center"}>
          <NavLink to="/">
            <Image src={images.logo} alt="Centoll" height="20px" />
          </NavLink>
          <HStack as={"nav"} spacing={4}>
            {routes.map((route) => (
              <NavLink
                to={route.path}
                key={route.path}
                activeStyle={{ fontWeight: 600, color: colors.brand.primary }}
              >
                <Text fontSize="0.95rem" letterSpacing="0.02rem">
                  {route.text}
                </Text>
              </NavLink>
            ))}
          </HStack>
        </HStack>
        <Flex alignItems={"center"}>
          <Menu>
            <MenuButton variant={"link"} cursor={"pointer"}>
              <HStack spacing={2}>
                <Avatar
                  rounded="full"
                  size="sm"
                  src={
                    localStorage.getItem("_centoll_email")! &&
                    gravatar.url(localStorage.getItem("_centoll_email")!, {
                      s: "100",
                    })
                  }
                />
                <Text fontSize="sm" fontWeight={600}>
                  {user?.firstName} {user?.lastName}
                </Text>
                <Icon as={IoChevronDownOutline} w={4} h={4} />
              </HStack>
            </MenuButton>
            <MenuList p={0}>
              {dropdownRoutes.map((route) => (
                <MenuItem key={route.path} py={2}>
                  <NavLink to={route.path}>
                    <Text
                      fontSize="sm"
                      letterSpacing="0.02rem"
                      fontWeight={500}
                    >
                      {route.text}
                    </Text>
                  </NavLink>
                </MenuItem>
              ))}
              <MenuDivider m={0} />
              <MenuItem
                as="button"
                color="red.500"
                onClick={() => logout()}
                py={2}
              >
                <Text fontSize="sm" letterSpacing="0.02rem" fontWeight={500}>
                  Logout
                </Text>
              </MenuItem>
            </MenuList>
          </Menu>
        </Flex>
      </Flex>
    </Box>
  );
};

export default NavBar;

/*
      <NavbarStyle onMouseLeave={() => setUserDropdownState(false)}>
        <div className="container navbar-container">
          <div className="navbar-container-logo">
            <Link to="/">
              <img src={images.logo} alt="" />
            </Link>
          </div>
          <ul className="navbar-container-items">
            {routes.map((route, routeIndex) => (
              <li key={`route-${routeIndex}`}>
                <NavLink
                  className="navbar-container-items-item"
                  to={route.path}
                  activeClassName="active"
                >
                  {route.text}
                </NavLink>
              </li>
            ))}
          </ul>
          <ul className="navbar-container-right">
            <li>
              <NavLink
                className="navbar-container-right-item"
                to="/help"
                activeClassName="active"
              >
                Help
              </NavLink>
            </li>
            <li className="navbar-container-right-item">
              {isLogged ? (
                <div
                  onMouseEnter={() => setUserDropdownState(true)}
                  className="navbar-container-right-item-user"
                >
                  <Avatar
                  image={
                    localStorage.getItem("_centoll_email")! &&
                    gravatar.url(localStorage.getItem("_centoll_email")!, {
                      s: "100",
                    })
                  }
                  user="Jaime Dordio"
                  alert={{ type: "error", message: "Notification" }}
                  rounded
                  dropdownArrow
                />
                <div
                className={`navbar-container-right-item-user__dropdown ${
                  userDropdownState ? "active" : ""
                }`}
              >
                <Link
                  className="navbar-container-right-item-user__dropdown__item"
                  to="/profile"
                >
                  My profile
                </Link>
                <div
                  className="navbar-container-right-item-user__dropdown__item logout"
                  onClick={() => logout()}
                >
                  Logout
                </div>
              </div>
            </div>
          ) : (
            <></>
          )}
        </li>
      </ul>
      <div
        className="navbar-container-drop-action"
        onClick={() => setResNavbarState(!resNavbarState)}
      >
        <SVGIcon size="32px">
          <IoIosReorder />
        </SVGIcon>
      </div>
    </div>




    <div className={`navbar-drop ${resNavbarState ? "active" : ""}`}>
      <ul className="navbar-drop-links">
        {routes.map((route, routeIndex) => (
          <li
            key={`route-${routeIndex}`}
            className="navbar-drop-links-item"
          >
            <NavLink to={route.path} activeClassName="active">
              {route.text}
            </NavLink>
          </li>
        ))}
      </ul>
      {isLogged && (
        <div
          onClick={() => setUserDropdownState(!userDropdownState)}
          className="navbar-drop-user"
        >
       <Avatar
          image={
            localStorage.getItem("_centoll_email")! &&
            gravatar.url(localStorage.getItem("_centoll_email")!, {
              s: "100",
            })
          }
          user="Jaime Dordio"
          alert={{ type: "error", message: "Notification" }}
          rounded
          dropdownArrow
        /> 
          <div
            className={`navbar-drop-user__dropdown ${
              userDropdownState ? "active" : ""
            }`}
          >
            <Link
              className="navbar-drop-user__dropdown__item"
              to="/profile"
            >
              My profile
            </Link>
            <div
              className="navbar-drop-user__dropdown__item logout"
              onClick={() => logout()}
            >
              Logout
            </div>
          </div>
        </div>
      )}
      <ul className="navbar-drop-bottom">
        <li className="navbar-drop-bottom-item">
          <NavLink to="/help" activeClassName="active">
            Help
          </NavLink>
        </li>
      </ul>
    </div>
  </NavbarStyle>
*/
