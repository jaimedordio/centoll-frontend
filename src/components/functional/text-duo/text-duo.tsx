import React, { ReactNode } from "react";
import { colors } from "../../../assets/colors/colors";
import { BodySmallText } from "../../styled/text/body-text";
import { TextDuoStyle } from "./text-duo.style";

interface ITextDuoProps {
  label: string;
  value: ReactNode;
}

const TextDuo: React.FC<ITextDuoProps> = ({ label, value }) => {
  return (
    <TextDuoStyle>
      <BodySmallText color={colors.bn.gray_500}>{label}</BodySmallText>
      <div>{value}</div>
    </TextDuoStyle>
  );
};

export default TextDuo;
