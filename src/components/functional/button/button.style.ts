import styled from "styled-components";
import { colors } from "../../../assets/colors/colors";

export const ButtonStyle = styled.button`
  cursor: pointer;
  width: fit-content;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: 0.3s ease-in-out all;

  .button-right-icon,
  .button-left-icon {
    width: 20px;
    height: 20px;
    min-width: 20px;
    min-height: 20px;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .button-left-icon {
    margin-right: 12px;
  }

  .button-right-icon {
    margin-left: 12px;
  }

  .button-loader {
    min-height: 32px;
    max-height: 32px;
    display: flex;
    justify-content: center;
    align-items: center;

    img {
      height: 32px;
    }
  }

  /*
   * ---------------------
   * Size
   * ---------------------
   */
  &.large {
    padding: 12px 24px;

    .button-text {
      font-size: 16px;
      font-weight: 600;
    }
  }

  &.medium {
    padding: 12px 20px;

    .button-left-icon {
      margin-right: 8px;
    }

    .button-text {
      font-size: 15px;
      font-weight: 500;
    }
  }

  &.small {
    padding: 8px 16px;

    .button-text {
      font-size: 14px;
    }
  }

  /*
   * ---------------------
   * Type
   * ---------------------
   */
  &.primary {
    border: 1px solid ${colors.brand.primary};
    background: ${colors.brand.primary};
    color: ${colors.bn.white};

    &:hover {
      border: 1px solid ${colors.states.hover.primary};
      background: ${colors.states.hover.primary};
    }

    &:active {
      border: 1px solid ${colors.states.active.primary};
      background: ${colors.states.active.primary};
    }

    svg {
      path {
        fill: ${colors.bn.white};
        stroke: ${colors.bn.white} !important;
      }

      line,
      polyline {
        stroke: ${colors.bn.white} !important;
      }
    }
  }

  &.secondary {
    border: 1px solid ${colors.brand.secondary};
    background: ${colors.brand.secondary};
    color: ${colors.bn.white};

    &:hover {
      background: ${colors.states.hover.secondary};
      border: 1px solid ${colors.states.hover.secondary};
    }

    &:active  {
      background: ${colors.states.active.secondary};
      border: 1px solid ${colors.states.active.secondary};
    }

    svg {
      path {
        fill: ${colors.bn.white};
      }
    }
  }

  &.tertiary {
    border: 1px solid ${colors.brand.tertiary};
    background: ${colors.brand.tertiary};
    color: ${colors.bn.white};

    &:hover {
      border: 1px solid ${colors.states.hover.tertiary};
    }

    &:active  {
      border: 1px solid ${colors.states.active.tertiary};
    }

    svg {
      path {
        fill: ${colors.bn.white};
      }
    }
  }

  &.neutral {
    border: 1px solid ${colors.bn.gray_200};
    background: ${colors.bn.white};
    color: ${colors.bn.black};

    &:hover {
      background: ${colors.bn.gray_100};
    }

    &:active  {
      background: ${colors.bn.gray_200};
    }

    svg {
      path {
        fill: ${colors.bn.black};
      }
    }
  }

  &.danger {
    border: 1px solid ${colors.states.standard.error};
    background: ${colors.states.standard.error};
    color: ${colors.bn.white};

    &:hover {
      border: 1px solid ${colors.states.hover.error};
      background: ${colors.states.hover.error};
    }

    &:active {
      border: 1px solid ${colors.states.active.error};
      background: ${colors.states.active.error};
    }

    svg {
      path {
        fill: ${colors.bn.white};
        stroke: ${colors.bn.white} !important;
      }

      line,
      polyline {
        stroke: ${colors.bn.white} !important;
      }
    }
  }

  &.success {
    border: 1px solid ${colors.states.standard.success};
    background: ${colors.states.standard.success};
    color: ${colors.bn.white};

    &:hover {
      border: 1px solid ${colors.states.hover.success};
      background: ${colors.states.hover.success};
    }

    &:active {
      border: 1px solid ${colors.states.active.success};
    }

    svg {
      path {
        fill: ${colors.bn.white};
        stroke: ${colors.bn.white} !important;
      }

      line,
      polyline {
        stroke: ${colors.bn.white} !important;
      }
    }
  }

  &.ghost {
    color: ${colors.brand.primary};
    padding: 0;

    .button-text {
      text-decoration: underline;
    }

    &:hover {
      color: ${colors.states.hover.primary};
    }

    &:active {
      color: ${colors.states.active.primary};
    }

    svg {
      path {
        fill: ${colors.brand.primary};
        stroke: ${colors.brand.primary} !important;
      }

      line,
      polyline {
        stroke: ${colors.brand.primary} !important;
      }
    }
  }

  &.ghost-danger {
    color: ${colors.states.standard.error};
    padding: 0;

    .button-text {
      text-decoration: underline;
    }

    &:hover {
      color: ${colors.states.hover.error};
    }

    &:active {
      color: ${colors.states.active.error};
    }

    svg {
      path {
        fill: ${colors.states.hover.error};
        stroke: ${colors.states.hover.error} !important;
      }

      line,
      polyline {
        stroke: ${colors.states.hover.error} !important;
      }
    }
  }

  /*
   * ---------------------
   * OHTERS
   * ---------------------
   */
  &.disabled {
    &:not(.ghost):not(.ghost-danger) {
      background: ${colors.bn.gray_200};
      border: 0px solid ${colors.bn.gray_200};
    }

    color: ${colors.bn.gray_300};
    pointer-events: none;
  }

  &.error {
    border: 1px solid ${colors.states.standard.error};
  }

  &.fullWidth {
    width: 100%;
  }
`;

export const ButtonIconStyle = styled(ButtonStyle)`
  padding: 8px;
  width: fit-content;
  border: 1px solid black;

  .button-icon {
    width: 32px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;
