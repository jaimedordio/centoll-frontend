import classnames from "classnames";
import React, { ReactNode } from "react";
import { images } from "../../../assets/images/images";
import SVGIcon from "../svg-icon/svg-icon";
import { ButtonIconStyle, ButtonStyle } from "./button.style";

interface IButtonProps {
  className?: string;
  disabled?: boolean;
  fullWidth?: boolean;
  isSubmit?: boolean;
  leftIcon?: ReactNode;
  leftIconColor?: string;
  loading?: boolean;
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
  rightIcon?: ReactNode;
  rightIconColor?: string;
  singleIcon?: ReactNode;
  size?: "large" | "medium" | "small";
  text?: string;
  type?:
    | "primary"
    | "secondary"
    | "tertiary"
    | "neutral"
    | "danger"
    | "success"
    | "ghost"
    | "ghost-danger";
}

const Button: React.FC<IButtonProps> = ({
  className,
  disabled,
  fullWidth,
  isSubmit,
  leftIcon,
  leftIconColor,
  loading,
  onClick,
  rightIcon,
  rightIconColor,
  singleIcon,
  size,
  text,
  type,
}) => {
  const onClickHandler = (
    ev: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    if (!disabled && onClick) onClick(ev);
  };

  return (
    <>
      {!singleIcon ? (
        <ButtonStyle
          data-testid="button"
          className={classnames(
            className || "",
            type || "primary",
            size || "large",
            {
              disabled: !!disabled,
              fullWidth: fullWidth,
            }
          )}
          onClick={(ev) => onClickHandler(ev)}
          type={isSubmit ? "submit" : "button"}
        >
          {!loading ? (
            <>
              {leftIcon && (
                <div className="button-left-icon">
                  <SVGIcon color={leftIconColor} size="16px">
                    {leftIcon}
                  </SVGIcon>
                </div>
              )}
              {text && (
                <div className="button-text">
                  <p>{text}</p>
                </div>
              )}
              {rightIcon && (
                <div className="button-right-icon">
                  <SVGIcon color={rightIconColor} size="16px">
                    {rightIcon}
                  </SVGIcon>
                </div>
              )}
            </>
          ) : (
            <div className="button-loader">
              <img src={images.puff} alt="" />
            </div>
          )}
        </ButtonStyle>
      ) : (
        <ButtonIconStyle
          className={classnames(className || "", type || "", {
            disabled: !!disabled,
          })}
          onClick={(ev) => onClickHandler(ev)}
        >
          <div className="button-icon">
            <SVGIcon size="16px">{singleIcon}</SVGIcon>
          </div>
        </ButtonIconStyle>
      )}
    </>
  );
};

export default Button;
