import styled, { css } from "styled-components";

interface ITypography {
  weight?: number;
  color?: string;
}

const BaseText = css<ITypography>``;

export const BodyLargeMixin = css`
  ${BaseText};
  font-weight: 400;
  font-size: 1.125rem;
  line-height: 28px;
`;

export const BodyLargeText = styled.p<ITypography>`
  ${BodyLargeMixin};
  color: ${(props) => props.color};
  font-weight: ${(props) => props.weight};
`;

export const BodyLargeBoldMixin = css`
  ${BaseText};
  ${BodyLargeMixin}
  font-weight: 700;
`;

export const BodyLargeBoldText = styled.p<ITypography>`
  ${BodyLargeBoldMixin};
  color: ${(props) => props.color};
  font-weight: ${(props) => props.weight};
`;

export const BodyDefaultMixin = css`
  ${BaseText};
  font-weight: 500;
  font-size: 1rem;
  line-height: 24px;
`;

export const BodyDefaultText = styled.p<ITypography>`
  ${BodyDefaultMixin};
  color: ${(props) => props.color};
  font-weight: ${(props) => props.weight};
`;

export const BodyDefaultBoldMixin = css`
  ${BaseText};
  ${BodyDefaultMixin};
  font-weight: 700;
`;

export const BodyDefaultBoldText = styled.p<ITypography>`
  ${BodyDefaultBoldMixin};
  color: ${(props) => props.color};
  font-weight: ${(props) => props.weight};
`;

export const BodySmallMixin = css`
  ${BaseText};
  font-weight: 500;
  font-size: 0.875rem;
  line-height: 20px;
`;

export const BodySmallText = styled.p<ITypography>`
  ${BodySmallMixin};
  color: ${(props) => props.color};
  font-weight: ${(props) => props.weight};
`;

export const BodySmallBoldMixin = css`
  ${BaseText};
  ${BodySmallMixin};
  font-weight: 700;
`;

export const BodySmallBoldText = styled.p<ITypography>`
  ${BodySmallBoldMixin};
  color: ${(props) => props.color};
  font-weight: ${(props) => props.weight};
`;

export const BodyXSmallMixin = css`
  ${BaseText};
  font-weight: 500;
  font-size: 0.75rem;
  line-height: 16px;
`;

export const BodyXSmallText = styled.p<ITypography>`
  ${BodyXSmallMixin};
  color: ${(props) => props.color};
  font-weight: ${(props) => props.weight};
`;

export const BodyXSmallBoldMixin = css`
  ${BaseText};
  ${BodyXSmallMixin};
  font-weight: 700;
`;

export const BodyXSmallBoldText = styled.p<ITypography>`
  ${BodyXSmallBoldMixin};
  color: ${(props) => props.color};
  font-weight: ${(props) => props.weight};
`;
