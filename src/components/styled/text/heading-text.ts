import styled, { css } from "styled-components";

interface ITypography {
  weight?: 300 | 400 | 500 | 600 | 700;
  color?: string;
}

const BaseHeading = css<ITypography>`
  margin: 0.625rem 0;
  color: ${(props) => props.color};
  font-weight: ${(props) => props.weight} !important;
`;

export const Heading1Mixin = css`
  ${BaseHeading};
  font-size: 2rem;
  letter-spacing: -0.066875rem;
  line-height: 1.5;
  font-weight: 700;
`;

export const Heading1Text = styled.h1<ITypography>`
  ${Heading1Mixin};
`;

export const Heading2Mixin = css`
  ${BaseHeading};
  font-size: 1.5rem;
  letter-spacing: -0.020625rem;
  font-weight: 700;
`;

export const Heading2Text = styled.h2<ITypography>`
  ${Heading2Mixin};
`;

export const Heading3Mixin = css`
  ${BaseHeading};
  font-size: 1.2rem;
  letter-spacing: -0.029375rem;
  font-weight: 700;
`;

export const Heading3Text = styled.h3<ITypography>`
  ${Heading3Mixin};
`;

export const Heading4Mixin = css`
  ${BaseHeading};
  font-size: 1.12rem;
  letter-spacing: -0.020625rem;
  font-weight: 700;
`;

export const Heading4Text = styled.h4<ITypography>`
  ${Heading4Mixin};
`;

export const Heading5Mixin = css`
  ${BaseHeading};
  font-size: 0.83rem;
  letter-spacing: -0.01125rem;
  font-weight: 700;
`;

export const Heading5Text = styled.h5<ITypography>`
  ${Heading5Mixin};
`;

export const Heading6Mixin = css`
  ${BaseHeading};
  font-size: 0.75rem;
  letter-spacing: -0.005625rem;
  font-weight: 700;
`;

export const Heading6Text = styled.h6<ITypography>`
  ${Heading6Mixin};
`;
