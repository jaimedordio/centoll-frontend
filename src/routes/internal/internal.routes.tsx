import { Box } from "@chakra-ui/react";
import React from "react";
import { Redirect, Route, Switch, useLocation } from "react-router-dom";
import styled from "styled-components";
import Footer from "../../components/functional/footer/footer";
import NavBar from "../../components/functional/navbar/navbar";
import Checkout from "../../containers/internal/checkout/checkout";
import OrderConfirmed from "../../containers/internal/order-confirmed/order-confirmed";
import OrderHistory from "../../containers/internal/order-history/order-history";
import Profile from "../../containers/internal/profile/profile";
import { ILocation } from "../../types/util_types";

const InternalRoutes: React.FC = () => {
  const { state } = useLocation<ILocation>();

  return (
    <InternalStyle>
      <NavBar />
      <Box className="container main-container">
        <Switch>
          <Route exact path="/checkout" component={Checkout} />
          <Route exact path="/order-history" component={OrderHistory} />
          <Route path="/profile" component={Profile} />
          {state?.orders && (
            <Route exact path="/order-confirmed" component={OrderConfirmed} />
          )}

          <Route path="/iframe">
            <div>iframe</div>
          </Route>
          <Redirect from="/" to="/checkout" />
        </Switch>
      </Box>
      <Footer />
    </InternalStyle>
  );
};

const InternalStyle = styled.div`
  background-color: #fbfbfb;

  .main-container {
    min-height: calc(100vh - 158.61px);
    margin: 24px auto;
  }
`;

export default InternalRoutes;
