import { Center, HStack, Image, Link, Text, VStack } from "@chakra-ui/react";
import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { images } from "../../assets/images/images";
import Login from "../../containers/auth/login/login";
import Signup from "../../containers/auth/signup/signup";

const AuthRoutes: React.FC = () => {
  return (
    <>
      <Center
        h="100vh"
        p={10}
        bgImage={`url(${images.shapesBg})`}
        bgSize="cover"
      >
        <VStack
          spacing={8}
          maxW="500px"
          w="500px"
          bgColor="white"
          borderRadius={4}
          p={12}
          alignItems="flex-start"
          alignSelf="center"
        >
          <Image src={images.logo} alt="Centoll" height="24px" />
          <VStack align="start" width="100%">
            <Switch>
              <Route exact path="/login" component={Login} />
              <Route exact path="/signup" component={Signup} />
              <Route component={Login} />
              <Redirect exact from="/" to="/login" />
            </Switch>
          </VStack>
          <HStack spacing={2} mt="100%" alignSelf="flex-end">
            <Link href="https://centoll.com">
              <Text fontSize="sm" fontWeight={500}>
                © Centoll
              </Text>
            </Link>
          </HStack>
        </VStack>
      </Center>
    </>
  );
};

export default AuthRoutes;
