import styled from "styled-components";
import { colors } from "../../assets/colors/colors";

export const AuthContainer = styled.div`
  /* background-color: ${colors.brand.primary}; */
  padding: 40px;
  height: 100vh;
  max-height: 100vh;
  min-height: 700px;
  position: relative;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-content: center;

  .auth-box {
    position: relative;
    z-index: 1;
    background-color: #ffffff;
    padding: 80px 40px 60px;
    border-radius: 8px;
    width: 508px;
    height: fit-content;
    align-self: center;
    display: flex;
    flex-direction: column;
    align-items: flex-start;

    .auth-box-logo {
      width: 120px;
      margin-bottom: 32px;
    }

    .auth-box-content {
      width: 100%;
    }

    .auth-box-bottom {
      align-self: flex-end;
      margin-top: 48px;
      display: flex;

      &__link {
        margin-right: 8px;

        a {
          font-size: 15px;
          font-weight: 500;
        }
      }
    }
  }

  .auth-bg {
    position: absolute;
    z-index: 0;
    bottom: 0;
    right: 0;
    height: 100%;
    width: 100%;

    img {
      object-fit: cover;
      height: 100%;
      width: 100%;
    }
  }

  @media all and (max-width: 768px) {
    padding: 16px;

    .auth-box {
      padding: 24px;
      min-width: unset;
      width: 100%;
    }
  }
`;
