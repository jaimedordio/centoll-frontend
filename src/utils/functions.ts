import Axios from "axios";
import { GroupedCart } from "../types/gql";

/**
 * Get items subtotal
 *
 * @param {GroupedCart[]} carts - Group of carts.
 *
 * @returns {number} Subtotal
 */
export const getItemsSubtotal = (carts: GroupedCart[]): number => {
  const reducedItems: number[] = [];

  carts.forEach((cart) =>
    reducedItems.push(
      ...cart.items!.map(
        (item) => parseFloat(item.price) * (item.quantity || 1)
      )
    )
  );

  const subtotal = reducedItems.reduce(
    (accumulator, current) => accumulator + current
  );

  return subtotal;
};

/**
 * Get items total taxes
 *
 * @param {GroupedCart[]} carts - Group of carts.
 *
 * @returns {number} Taxes
 */
export const getItemsTaxes = (carts: GroupedCart[]): number => {
  const reducedItems: number[] = [];

  carts.forEach((cart) =>
    reducedItems.push(
      ...cart.items!.map(
        (item) => parseFloat(item.price) * (item.quantity || 1)
      )
    )
  );

  const taxes = reducedItems.reduce(
    (accumulator, current) => accumulator + current
  );

  return taxes;
};

/**
 *
 * @param {string[]} domains - Array of domains for fetch
 *
 * @returns {Promise<GetFaviconReturn>} Object with favicons
 */

interface GetFaviconReturn {
  [key: string]: string;
}

export const getFavicons = async (
  domains: string[]
): Promise<GetFaviconReturn> => {
  const favicons: GetFaviconReturn = {};

  if (domains.length)
    for (const domain of domains) {
      try {
        const favicon = await Axios.get(
          `http://favicongrabber.com/api/grab/${domain}`
        );

        if (favicon.data.icons.length > 0) {
          favicons[domain] = favicon.data.icons[0].src;
        } else {
          favicons[domain] = "";
        }
      } catch (error) {
        console.error("Error fetching favicons", error);
      }
    }
  return favicons;
};
