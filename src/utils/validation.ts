import { isEmail, isPhoneNumber, isURL } from "class-validator";

// EMPTY
export const isEmpty = (value: any) =>
  value === null ||
  value === undefined ||
  value === "" ||
  (typeof value === "object" && Object.keys(value).length === 0);

// EMAIL
export const isValidEmail = (mail: string) => {
  return isEmail(mail);
};

// DATE
export const isValidDate = (date: string) =>
  /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/.test(date);

export const isValidNumber = (number: string) => {
  if (number.match(/^\d+$/) || number.match(/^\d+\.\d+$/)) {
    return true;
  }
  return false;
};

// PHONE INTERNATIONAL
export const isValidPhone = (number: string) => {
  return number ? isPhoneNumber(number, "ZZ") : true;
};

// PASSWORD
export const isValidPassword = (text: string) => {
  return text.length > 5;
};

export const hasNumber = (text: string) => {
  return /\d/.test(text);
};

// TIME FORMAT
export const isValidTime = (text: string) => {
  return /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(text);
};

// VALID FILE SIZE
export const isValidSize = (file: File) => {
  if (!file.type) return false;
  if (file.size > 6000000) {
    return false;
  }
};

// VALID URL
export const isValidUrl = (text: string) => {
  if (text) {
    return isURL(text, {
      require_protocol: true,
      require_valid_protocol: true,
      allow_trailing_dot: false,
      require_host: true,
      disallow_auth: true,
      require_tld: true,
      protocols: ["https"],
    });
  }
};
