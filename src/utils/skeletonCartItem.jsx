import React, { useEffect, useState } from "react";

import CurrencyNumber from "../utils/currencyNumber";

const SkeletonCartItem = ({
  type = "short",
  pulseDelay = "0ms",
  animationFillMode = "unset",
}) => {
  return (
    <div
      className="flex flex-row h-32 p-4 animate-pulse"
      style={{
        animationDelay: `${pulseDelay}`,
        animationFillMode: `${animationFillMode}`,
      }}
    >
      <div className="mr-6 bg-gray-200 w-28" />
      <div className="flex flex-row justify-between w-full">
        <div className="flex flex-col justify-start w-full mr-14">
          {type === "short" ? (
            <div className="w-5/12 h-5 bg-gray-200 rounded" />
          ) : type === "medium" ? (
            <div className="w-8/12 h-5 bg-gray-200 rounded" />
          ) : (
            <div className="w-10/12 h-5 bg-gray-200 rounded" />
          )}
          <div className="w-3/12 h-3 mt-4 bg-gray-200 rounded" />
          <div className="w-4/12 h-3 mt-4 bg-gray-200 rounded" />
        </div>
        <div className="w-20 h-5 ml-10 bg-gray-200 rounded" />
      </div>
    </div>
  );
};

export default SkeletonCartItem;
