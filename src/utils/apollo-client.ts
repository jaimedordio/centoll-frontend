import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { createApolloFetch } from "apollo-fetch";
import { onError } from "apollo-link-error";
import { SERVICE_URL } from "../constants/env";

const httpLink = new HttpLink({
  uri: SERVICE_URL,
});

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem("_centoll_jwt");

  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    },
  };
});

const errorLink = onError(
  ({ graphQLErrors, networkError, forward, operation, response }) => {
    if (networkError) {
      console.log(`[Network error]: ${networkError}`);
    }
  }
);

export const apolloClient = new ApolloClient({
  link: errorLink.concat(authLink.concat(httpLink) as any) as any,
  // link: ApolloLink.from({ links: [errorLink, httpLink] }),
  cache: new InMemoryCache({ addTypename: false }),
});

export const apolloFetch = createApolloFetch({
  uri: SERVICE_URL,
});

apolloFetch.use(({ request, options }, next) => {
  const token = localStorage.getItem("_centoll_jwt");

  options.headers = {
    authorization: token ? `Bearer ${token}` : "",
  };

  next();
});
