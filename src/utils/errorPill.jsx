import React from "react";

const ErrorPill = (props) => {
  const { errorMsg } = props;

  return (
    <div>
      <div className="my-4 text-center">
        <div
          className="flex items-center w-full p-2 leading-none text-red-100 bg-red-700 rounded-full lg:inline-flex"
          role="alert"
        >
          <div className="flex px-1 py-1 mr-3 text-xs font-semibold uppercase bg-red-500 rounded-full">
            <svg
              className="w-4 h-4 text-white"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          </div>
          <span className="flex-auto mr-2 text-sm text-left font-base">
            {errorMsg}
          </span>
        </div>
      </div>
    </div>
  );
};

export default ErrorPill;
