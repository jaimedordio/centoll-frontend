const CurrencyNumber = (props) => {
  return (
    <>
      {/* {typeof props.amount === "string"
        ? props.amount
        : Intl.NumberFormat(props.locales, {
            style: "currency",
            currency: props.currency,
          }).format(props.amount)} */}
      {Intl.NumberFormat(props.locales, {
        style: "currency",
        currency: props.currency,
      }).format(props.amount)}
    </>
  );
};

export default CurrencyNumber;
