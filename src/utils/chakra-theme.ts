import { extendTheme } from "@chakra-ui/react";

export const chakraCustomTheme = extendTheme({
  config: {
    cssVarPrefix: "c",
    initialColorMode: "light",
    useSystemColorMode: false,
  },
  colors: {
    transparent: "transparent",
    black: "#000",
    white: "#fff",
  },
});
