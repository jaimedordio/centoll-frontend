import toast from "react-hot-toast";

export const notify = (options: {
  message?: string;
  type?: "success" | "error" | "loading" | "dismiss";
  toastId?: string;
}) => {
  switch (options.type) {
    case "success":
      toast.success(options.message!);
      break;

    case "error":
      toast.error(options.message!);
      break;

    case "loading":
      return toast.loading(options.message!);

    case "dismiss":
      toast.dismiss(options.toastId);
      break;

    default:
      toast(options.message!);
      break;
  }
};
