interface IColorBrand {
  primary: string;
  secondary: string;
  tertiary: string;
}
interface IColorBN {
  black: string;
  gray_900: string;
  gray_800: string;
  gray_700: string;
  gray_600: string;
  gray_500: string;
  gray_400: string;
  gray_300: string;
  gray_200: string;
  gray_100: string;
  white: string;
}

interface IColorState {
  standard: {
    error: string;
    success: string;
    warning: string;
  };
  dark: {
    error: string;
    success: string;
    warning: string;
  };
  light: {
    error: string;
    success: string;
    warning: string;
  };
  hover: {
    primary: string;
    secondary: string;
    tertiary: string;
    error: string;
    success: string;
  };
  active: {
    primary: string;
    secondary: string;
    tertiary: string;
    error: string;
    success: string;
  };
}

interface ILowOpacityColorBrand {
  primary: string;
  secondary: string;
  tertiary: string;
  error: string;
  success: string;
  warning: string;
}

interface IColor {
  brand: IColorBrand;
  bn: IColorBN;
  states: IColorState;
  lowOpacity: ILowOpacityColorBrand;
}

export const colors: IColor = {
  brand: {
    primary: "rgba(2, 128, 218, 1)",
    secondary: "rgba(204, 91, 29,1)",
    tertiary: "rgba(0, 152, 144, 1)",
  },
  bn: {
    black: "rgba(0, 0, 0, 1)",
    gray_900: "rgba(17, 17, 17, 1)",
    gray_800: "rgba(51, 51, 51, 1)",
    gray_700: "rgba(68, 68, 68, 1)",
    gray_600: "rgba(102, 102, 102, 1)",
    gray_500: "rgba(136, 136, 136, 1)",
    gray_400: "rgba(153, 153, 153, 1)",
    gray_300: "rgba(234, 234, 234, 1)",
    gray_200: "rgba(245, 245, 245, 1)",
    gray_100: "rgba(250, 250, 250, 1)",
    white: "rgba(255, 255, 255, 1)",
  },
  states: {
    standard: {
      error: "rgba(255, 59, 48, 1)",
      success: "rgba(40, 205, 65, 1)",
      warning: "rgba(255, 212, 89, 1)",
    },
    dark: {
      error: "rgba(255, 69, 58, 1)",
      success: "rgba(50, 215, 75, 1)",
      warning: "rgba(255, 222, 99, 1)",
    },
    light: {
      error: "rgba(255, 226, 229, 1)",
      success: "rgba(200, 240, 207, 1)",
      warning: "rgba(255, 245, 215, 1)",
    },
    hover: {
      primary: "rgba(0, 108, 198, 1)",
      secondary: "rgba(184, 71, 9, 1)",
      tertiary: "rgba(0, 132, 124, 1)",
      error: "rgba(235, 39, 28, 1)",
      success: "rgba(20, 185, 45, 1)",
    },
    active: {
      primary: "rgba(0, 88, 188, 1)",
      secondary: "rgba(164, 51, 0, 1)",
      tertiary: "rgba(0, 112, 104, 1)",
      error: "rgba(215, 19, 8, 1)",
      success: "rgba(0, 165, 25, 1)",
    },
  },
  lowOpacity: {
    primary: "rgba(2, 128, 218, 0.2)",
    secondary: "rgba(204, 91, 29,0.2)",
    tertiary: "rgba(0, 152, 144, 0.2)",
    error: "rgba(255, 59, 48, 0.2)",
    success: "rgba(40, 205, 65, 0.2)",
    warning: "rgba(255, 212, 89, 0.2)",
  },
};
