import blob_01 from "./files/blob_01.svg";
import centoll_logo from "./files/centoll_logo.svg";
import affirm from "./files/payment-methods/Affirm.svg";
import alipay from "./files/payment-methods/Alipay.svg";
import amazonPay from "./files/payment-methods/AmazonPay.svg";
import amex from "./files/payment-methods/Amex.svg";
import applePay from "./files/payment-methods/ApplePay.svg";
import bancontact from "./files/payment-methods/Bancontact.svg";
import bitcoin from "./files/payment-methods/Bitcoin.svg";
import bitcoinCash from "./files/payment-methods/BitcoinCash.svg";
import bitpay from "./files/payment-methods/Bitpay.svg";
import citadele from "./files/payment-methods/Citadele.svg";
import dinersClub from "./files/payment-methods/DinersClub.svg";
import discover from "./files/payment-methods/Discover.svg";
import elo from "./files/payment-methods/Elo.svg";
import etherium from "./files/payment-methods/Etherium.svg";
import forbrugsforeningen from "./files/payment-methods/Forbrugsforeningen.svg";
import giropay from "./files/payment-methods/Giropay.svg";
import googlePay from "./files/payment-methods/GooglePay.svg";
import ideal from "./files/payment-methods/Ideal.svg";
import interac from "./files/payment-methods/Interac.svg";
import jcb from "./files/payment-methods/JCB.svg";
import klarna from "./files/payment-methods/Klarna.svg";
import lightcoin from "./files/payment-methods/Lightcoin.svg";
import maestro from "./files/payment-methods/Maestro.svg";
import mastercard from "./files/payment-methods/Mastercard.svg";
import payoneer from "./files/payment-methods/Payoneer.svg";
import payPal from "./files/payment-methods/PayPal.svg";
import paysafe from "./files/payment-methods/Paysafe.svg";
import qiwi from "./files/payment-methods/Qiwi.svg";
import sepa from "./files/payment-methods/SEPA.svg";
import shopPay from "./files/payment-methods/ShopPay.svg";
import skrill from "./files/payment-methods/Skrill.svg";
import sofort from "./files/payment-methods/Sofort.svg";
import stripe from "./files/payment-methods/Stripe.svg";
import unionPay from "./files/payment-methods/UnionPay.svg";
import verifone from "./files/payment-methods/Verifone.svg";
import visa from "./files/payment-methods/Visa.svg";
import webmoney from "./files/payment-methods/Webmoney.svg";
import weChat from "./files/payment-methods/WeChat.svg";
import yandex from "./files/payment-methods/Yandex.svg";
import puff from "./files/puff.svg";
import shapesBg from "./files/shapes-bg.png";
interface IImages {
  [key: string]: string | object;
  logo: string;
  blob_01: string;
  puff: string;
  shapesBg: string;
  paymentMethods: {
    [key: string]: string;
    affirm: string;
    alipay: string;
    amazonPay: string;
    amex: string;
    applePay: string;
    bancontact: string;
    bitcoin: string;
    bitcoinCash: string;
    bitpay: string;
    citadele: string;
    dinersClub: string;
    discover: string;
    elo: string;
    etherium: string;
    forbrugsforeningen: string;
    giropay: string;
    googlePay: string;
    ideal: string;
    interac: string;
    jcb: string;
    klarna: string;
    lightcoin: string;
    maestro: string;
    mastercard: string;
    payoneer: string;
    payPal: string;
    paysafe: string;
    qiwi: string;
    sepa: string;
    shopPay: string;
    skrill: string;
    sofort: string;
    stripe: string;
    unionPay: string;
    verifone: string;
    visa: string;
    webmoney: string;
    weChat: string;
    yandex: string;
  };
}

export const images: IImages = {
  logo: centoll_logo,
  blob_01,
  puff,
  shapesBg,
  paymentMethods: {
    affirm,
    alipay,
    amazonPay,
    amex,
    applePay,
    bancontact,
    bitcoin,
    bitcoinCash,
    bitpay,
    citadele,
    dinersClub,
    discover,
    elo,
    etherium,
    forbrugsforeningen,
    giropay,
    googlePay,
    ideal,
    interac,
    jcb,
    klarna,
    lightcoin,
    maestro,
    mastercard,
    payoneer,
    payPal,
    paysafe,
    qiwi,
    sepa,
    shopPay,
    skrill,
    sofort,
    stripe,
    unionPay,
    verifone,
    visa,
    webmoney,
    weChat,
    yandex,
  },
};
